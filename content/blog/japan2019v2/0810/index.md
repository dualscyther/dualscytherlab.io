---
title: A lazy start
date: "2019-08-10"
---

> It's amazing to just wake up and not do much...

Last time I was in Japan it was winter and both my sweat pants and down jacket
had many zipped pockets. It was amazing being able to carry my passport, JR
pass, wallet, phone, and coins (you end up with so many here) and not fretting
over things falling out. This time, I also need to carry keys because our
apartment doesn't use keyless entry, which is also annoying because there's only
one set of keys so we need to work out who's coming home first, and the unit
door (there's also a building door) doesn't lock without a key, so sometimes we
just have to leave our door unlocked. Ideally I'd like to stuff the pocket wifi
in my... pocket, too. However, none of my shorts have zipped pockets, and my
t-shirt certainly doesn't. Looks like I need to buy a tiny backpack or bumbag of
some sort.

I got 8 hours of sleep and woke up at 10, sitting on my laptop learning Japanese
until 12. We left the house at around 1.30 after Elbert woke up, having decided
to get food at an udon chain named Hanamura, in Ikebukuro—apparently one of the
busier Tokyo districts, and only one stop away.

![Bic Camera, Ikebukuro: unintentional Windows 7 pun.](./IMG_20190810_140334.jpg)

![Hanamaru, Ikebukuro: cold udon with onsen egg (buried underneath my garlic) + crab and eggplant tempura.](./IMG_20190810_143310.jpg)

![Hanamaru, Ikebukuro: a very happy high chair.](./IMG_20190810_145444.jpg)

We walked aimlessly around Sunshine City, Ikebukuro's main mall which famously
has a Pokemon Center, and walked around the streets to check out a few anime
stores for Neon Genesis Evangelion (an anime I like) merchandise but couldn't
find any.

![Sunshine City, Ikebukuro: Weathering With You promo that I saw.](./IMG_20190810_152110.jpg)

![Pokemon Center, Ikebukuro: I assume it's eternally busy here, like the crowd of people at the Disneyland merch store after all the rides close.](./IMG_20190810_152545.jpg)

It's so unbearably hot and humid in the Tokyo summer climate (you feel moist and
receive a blast of hot air as soon as you walk out of an air conditioned
environment) right now. On this particular day it was 33C with 65% humidity, so
we went to a 7-11[^1] where I got milk pudding and milk tea.

![7/11, Ikebukuro: seeing this dessert aisle again brought back memories.](./IMG_20190810_162015.jpg)

![7/11, Ikebukuro: yep, missed the taste of conbini pudding.](./IMG_20190810_162554.jpg)

It was about 6 by the time that we got home; Elbert played Fire Emblem on his
Switch while I started preparing to write this blog and learnt kanji on
[WaniKani](https://www.wanikani.com). Since we got up and ate lunch so late, we
only went out for dinner at 10.30. We walked around the neighbourhood and
settled on Yoshinoya—a chain known for dons—since it was surprisingly dead in
the area; I had a large gyuudon (beef don) with shallots and raw (or almost
raw??) egg.

![Yoshinoya, Ootsuka: gyuudon where the toppings were served deconstructed for whatever reason. The sieve on the bottom left is to pour the egg through to separate out the yolk. Seems like a lot of washing up to do!](./IMG_20190810_232518.jpg)

![Post egg pour. Perfectly on centre.](./IMG_20190810_232749.jpg)

We stopped at the local Lawson on the way home and I got a warabimochi[^2] cream
roll (it wasn't great, the mochi tasted more like cake and it was all a bit
heavy) and a cheap Kirin beer to finish off most of my remaining coins. I spent
the rest of the night chilling out on my laptop (i.e. checking OzBargain and
doing WaniKani) and finally starting to write [yesterday's blog](../0809/)
rather than just making notes or fixing little website engineering issues.

![Home: warabimochi roll.](./IMG_20190811_004614.jpg)

![The only beer that I could buy with my remaining coins.](IMG_20190811_010551.jpg)

It's amazing to just wake up and not do much instead of waking up early and
rushing to go somewhere because you want to make the most out of your holiday
day. It's pretty much like sleeping in on a free day during the uni semester.

[^1]:

  damn, didn't last 24 hours without going to a _conbini_ (convenience store)

[^2]:

  according to [Wikipedia](https://en.wikipedia.org/wiki/Warabimochi) it's made
  from bracket starch rather than glutinous rice

##### Waking hours

1000–2800 (I've seen a few shop opening hours here that are written like this).
