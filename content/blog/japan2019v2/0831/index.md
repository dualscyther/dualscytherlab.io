---
title: Ueno Park
date: "2019-08-31"
---

My sleeping mask and earplugs get uncomfortable after a while, so it's been hard
to sleep with them unless I'm just taking a nap. It might just be because I got
a cheap mask from Daiso. I probably won't be using them to sleep.

I left at 1620 to go to Ueno Park, which was on my list of things to see.

![Ueno Park, Ueno: I love these maps. So detailed, so easy to read, and
multilingual.](./IMG_20190831_164148.jpg)
![Statue of Saigo Takamori, the "last true samurai".](./IMG_20190831_164700.jpg)

I needed breakfast so I went to Pepper Lunch to see how it compared to Sydney.
It's actually a steakhouse here, serving a variety of different steaks, as well
as drinks and wine. The pepper rice is relegated to the back page.

![Pepper Lunch, Ueno Park: the plate must've been super hot because I spent so
much effort just making sure nothing burned. Plus the oil spat all over me, as
usual. At least they give you a paper bib.](./IMG_20190831_165736.jpg)
![Yum. Tastes about the same as in Sydney.](./IMG_20190831_170207.jpg)

After food it was back to exploring. It was either earlier than I was used to
(so temperatures hadn't dropped yet) or it was more humid than the last few days
because I worked up a good sweat walking around. I also had my legs bitten by
what I assume to be mosquitoes because they were damn itchy.

![Look at that tree.](./IMG_20190831_172934.jpg)
![Bentendo Temple, Ueno Park: a temple on an island.](./IMG_20190831_173421.jpg)
![Looking back over the bridge I crossed. The approach is full of street
vendors.](./IMG_20190831_173326.jpg)
![Fortunes and nice trees.](./IMG_20190831_173455.jpg)
![View from the back. Few people here need to actually watch their head on that
underpass.](./IMG_20190831_173641.jpg)
![On a bridge on the other side of the island. Fish!](./IMG_20190831_173721.jpg)
![These are growing in the water as far as you can see.](./IMG_20190831_173734.jpg)
![Looking out towards the Boat Pond.](./IMG_20190831_173950.jpg)
![Walking back through the island again. I wonder if those plants grow in the
water all year round, because they sort of block the view.](./IMG_20190831_174307.jpg)
![This park is huge. Part of it is a zoo.](./IMG_20190831_174558.jpg)
![It was after 1700 so this was closed.](./IMG_20190831_174833.jpg)
![As was this.](./IMG_20190831_175022.jpg)
![But I got a nice photo anyway.](./IMG_20190831_175005.jpg)
![Most of the footpaths are so wide.](./IMG_20190831_175144.jpg)
![So many random little sites to see in this park.](./IMG_20190831_175641.jpg)

![Another shrine. This one was closed too, but I don't really mind too much
because to my eyes they all start looking the same at some point.](./IMG_20190831_175908.jpg)
![A bit spooky here. The canopy overhead made it dark, there was a hollow bell
ringing to signal 1800, and the sound of the cicadas was dampened.](./IMG_20190831_180009.jpg)
![A picture through the bars.](./IMG_20190831_180203.jpg)
![ ](./IMG_20190831_180352.jpg)
![Five Storied Pagoda, just inside the zoo. How do these stay up in typhoons?](./IMG_20190831_180410.jpg)
![There were plenty of engraved stones around the shrines in the park. I just
took a picture of this one.](./IMG_20190831_180456.jpg)
![These stairs alternate large and small steps. Incredibly annoying to climb
down.](./IMG_20190831_180553.jpg)

![Caught a team holding baseball practice. Some other tourists were watching
too, and one or two people who seemed like locals.](./IMG_20190831_181907.jpg)
![Starbucks on the left, another cafe on the right, and this in front.](./IMG_20190831_182637.jpg)
![Most of the shops were Thai, which leads me to believe that this was some sort
of Thai (or at least SEA) event. One stall even sold Durian, while others sold
SEA style clothing, and another still had Thai massages. I was thirsty and
bought a lychee shake which had as much ice as it did shake.](./IMG_20190831_183240.jpg)

![A place to sit, relax, and eat.](./IMG_20190831_183921.jpg)
![View from the other side of the fountain. How often do they replace the
flowers?](./IMG_20190831_184112.jpg)
![Tokyo National Museum, Ueno: the view from Ueno Park across the road. I might
come here another day.](./IMG_20190831_184310.jpg)

After a few hours checking out the park, I walked to Ueno to have dinner.

![Near Ueno JR Station, Ueno: this reminds me of the Flatiron Building.](./IMG_20190831_190904.jpg)
![Yodobashi Camera, Ueno: I bought plenty of these "Huzzle" toys last time I was
in Japan and I'll probably buy more. These are so fun. I wasted half an hour
here just playing with them.](./IMG_20190831_200813.jpg)
![A sushi restaurant, Ueno: that menu on the right is literally the English menu
that they give to you, but way bigger.](./IMG_20190831_204724.jpg)

I sat at the counter where a few chefs were working, making the sushi in front
of me. The chef in front of me couldn't speak much English but he did manage to
point at a menu item and say "set menu".

![A 1600 JPY nigiri set. So much better than Sydney for this price.](./IMG_20190831_205452.jpg)

The squid was chewy (not horribly so) as expected. The salmon roe was fine, not
fishy. The uni was fine too, it had the familiar slight fishy taste but I liked
it. Maybe I'm acquiring a taste for uni too. The egg piece was big, it needed 2
bites. The ootoro was probably the fattiest piece that I've had since I've been
here, but it wasn't particularly flavoursome.

Throughout my meal, the chef made eye contact with me and smiled a few times,
clearly wanting to see how I was doing or wanting to make conversation, but my
poor Japanese and his poor English let us down. He did ask me "oishii?" (tasty?)
and I said "oishii", although I'm not sure you're ever meant to respond
otherwise anyway.

I needed to delay ten minutes or so before I went home, since Elbert had the
keys and he was out, so I sat outside Ueno station listening to a busker. I
ended up staying for twenty minutes because she had an amazing voice.

![Home, Ootsuka: these were waiting for me to eat them.](./IMG_20190901_000418.jpg)
![Cut irregularly with a bad knife and bad knife skills, unpeeled, and unsalted.
Maybe next time I'll buy some more ingredients and kitchenware, and make soup.](./IMG_20190901_035215.jpg)
![Okay, this definitely needed salt.](./IMG_20190901_035450.jpg)

##### Waking hours

1500–2920
