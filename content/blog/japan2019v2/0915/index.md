---
title: Do nothing rest day
date: "2019-09-15"
---

I left the house at 1515 for breakfast.

![Mos Burger, Ootsuka: fish burger. These burgers are small but I'm pretty
sure it was still bigger than a Maccas
Filet-O-Fish.](./IMG_20190915_153208.jpg)
![Cheaper and better than Maccas (Sydney) too, since it had onion.](./IMG_20190915_153436.jpg)
![I bought some chip/cracker/sticks at the local Don Quijote.](./IMG_20190915_161449.jpg)
![And these really soft peach gummies. I had a Pulp Fiction moment where I
kept picking up gummy packets, seeing a more appealing one, then
switching.](./IMG_20190915_161459.jpg)

I finished the chips (which were way too sweet) and half the gummies when I got
home. The sugar probably contributed to me going back to sleep for an hour until
about 1800.

Elbert and I got hungry at some point and went out for dinner. We first walked
to Nakiryu, a ramen restaurant with a Michelin star. The line was long and the
restaurant didn't seat many people so we found an okonomiyaki restaurant
instead.

![Tena, Ootsuka: kimchi and pork straight off the hot plate. Delicious.](./IMG_20190915_192957.jpg)
![Okonomiyaki.](./IMG_20190915_193246.jpg)
![We didn't know how to cook it and I caught a girl at the table next to us
sneakily looking at us, laughing, and whispering to her boyfriend. It was a
bit scary because there was chicken in it, and we sort of just tried to make
the pancake straight away rather than cooking everything first, then pouring
in the sauce.](./IMG_20190915_193547.jpg)
![Still looks good with mayo on it. The sauce bottle had a head that made you
squeeze out five thin lines at a time.](./IMG_20190915_195105.jpg)
![Monjayaki. We didn't know the difference and thought that it was eaten the
same way. This time one of the waiters, who saw that we failed to cook the
okonomiyaki properly, showed us how to cook it. When it was ready to eat, he
told us that we could use our little spoons, but since it looked like it
wasn't quite ready yet, we assumed he meant that we use our little spoons to
prod it and shrink it somehow as it cooked. We did this for at least ten
minutes, until the couple next to us probably couldn't bear to watch any
longer, and kindly told us that we were just supposed to drag little
spoonfuls off to eat. The waiter saw this interaction and realised his
mistake of assuming that we had the faintest idea about what we were eating,
and apologised to us. What a disaster.](./IMG_20190915_202251.jpg)

I can't say I'm the biggest fan of frying stuff right in front of your face to
be honest. I don't like the feeling of oil getting everywhere. I understand that
cooking your own food right in front of you is what actually draws people in,
but it's not for me. Too much effort! Even the mental effort from having to
choose what sauces and toppings to add to your food once it's already at the
table, I find annoying.

![Home, Ootsuka: finishing these grapes which mainly tasted like skin.](./IMG_20190915_232636.jpg)
![Feeling hungry so I had a Lawson pasta. The chilli added a nice sweetness
to it.](./IMG_20190916_005938.jpg)

It feels amazing to take a break after a few jam packed days, although I can't
take a break from blogging!

##### Waking hours

1230–2840
