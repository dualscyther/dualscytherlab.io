---
title: Tokyo Station, sorta
date: "2019-09-18"
---

I left the house at 1305, hoping to explore the area around Tokyo Station and
Nihombashi.

![Tokyo station, Marunouchi: the way that the 7-segment displays refresh
meant that I couldn't get a picture of the numbers properly, but it says 6
hours.](./IMG_20190918_133756.jpg)

After exiting the station I looked for food at the nearest big building, which
happened to be a shopping mall with offices on the upper levels, a type of
building which feels really common here due to the sheer number of tall towers
that exist.

![Kitte, Marunouchi: this mall had this nice wide open area.](./IMG_20190918_134216.jpg)
![Saza Coffee, Kitte: err yeah look at those specialty coffee prices on
the left. They had every brew method available and on top of selling the
usual coffee paraphernalia and beans, they also sold traditional kissaten
cups and saucers. The waiters wore shirts, ties, and those diner hats, so it
had a traditional vibe but with modern coffee
available.](./IMG_20190918_134532.jpg)
![Luckily my Brazil medium roast espresso was only 380 JPY. It was really
caramelly which was perfectly balanced by its acidity. Full bodied, not the
typical clear tasting light roast that I love but it was great anyway. I was
surprised that I didn't get a glass of sparkling to go with it since that
tends to be the norm (not that I care either way). Maybe I should've had a
siphon brew since I haven't had one here this
trip.](./IMG_20190918_135409.jpg)

Well I was in a shopping centre already, so I took a while to walk around every
level and browse a lot of the shops.

![Kitte, Marunouchi: some heritage room I'm guessing? It seems that you can
chill out here if you want, and there are some pens provided for you to write
letters with. Not sure what that's all about.](./IMG_20190918_150812.jpg)
![ ](./IMG_20190918_150827.jpg)

![Reading Style, Kitte: this cafe was also a bookstore and I think you could
borrow books off the shelves to read them at the tables. I didn't think that
it was a good idea to do it while eating though. I had eggplant, egg and rice
with salad. The food was good. I ordered a royal milk tea and paid the extra
50 JPY to have it hot, but it turned out to be a waste because they steamed
it way too hot and probably ruined the milk. I also saw a lot of office
workers coming down to eat for lunch. It must be nice being able to take the
lift down and be within 50m of dozens of
restaurants.](./IMG_20190918_152421.jpg)
![Kitte, Marunouchi: a little outdoor terrace. It was raining so I didn't
spend long out here.](./IMG_20190918_161948.jpg)
![View of Tokyo station from the terrace.](./IMG_20190918_162007.jpg)

Reading back on my notes I realise now that I completely forgot about my plans
to go to Nihombashi. Instead, I sat down at the communal seats and tables on the
ground floor (pretty rare for those to exist in Tokyo other than in parks),
learnt Japanese on my phone, and watched office workers come out of the lifts
after their workday. Apparently Marunouchi (the side of Tokyo station which I
exited) is Tokyo's financial district but I didn't take a good look since I
holed up in Kitte all day and it was raining outside.

After getting sick of learning Japanese, I went back to Tokyo station and
explored one of the big malls connected to the station building. I ended up in
the Tokyu Hands stationery section and I must've spent 30-60 minutes there. I
was looking for a nice refillable notebook but I could only find journals and
planners. They also had a huge pen section, predominantly from Japanese
manufacturers but also including all the non-Japanese big names too. None of the
pens were "cheap" and were mainly sold individually. I didn't look at the prices
too much, but I'd say that most pens were in the 200-400 JPY range, with more
expensive metal ones selling for 500-1500. They had a particularly large display
of Uni Jetstream pens with heaps of different varieties, some plastic, some
metal (and expensive), some short, many colours, and some multicolour ones which
could include a mechanical pencil.

Unfortunately, the store closed while I was still browsing so I didn't buy
anything. Elbert happened to be in the area so I met up with him, but he wasn't
hungry so he went home first while I stayed behind to eat.

![Some Kyoto inspired restaurant, Tokyo station: I ordered a tea chazuke set
and they gave pickles to start. This is the first time that I've had pickled
pumpkin but I wasn't really a fan since there was no pumpkin taste. The other
vegetables had light pickling so you could still make our their original
flavours and it was delicious.](./IMG_20190918_203242.jpg)
![The waiter poured water into the pot on the stove in the middle of the
table. The teapot had green tea leaves and the lidded container had white
rice. I was supposed to scoop hot water into the teapot, put all the food
into the mixing bowl on the bottom left, then pour the tea on top. I liked
the clean taste but it wasn't as amazing as the last chazuke I had, probably
due to the mild taste of the tea. The fish was good and tasted a bit like
lapcheung (Chinese sausage).](./IMG_20190918_204305.jpg)

I got home at 2200 and stayed up doing who knows what, and blogging. I bought a
pudding at Lawson and the man at the counter (who I've seen many times before)
asked me where I was from (at first in Japanese but I didn't understand him) and
I said Australia. He told me that he was from Nepal. Maybe I'll talk to him more
on my next midnight snack trip.

![Home, Ootsuka: I had high expectations for this milk flavour, which I
assume to be the default/base flavour.](./IMG_20190919_022225.jpg)
![It absolutely delivered. Tasted like milk, and incredibly creamy. What more
do you need?](./IMG_20190919_022325.jpg)

##### Waking hours

1130–2750
