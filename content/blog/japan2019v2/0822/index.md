---
title: Watching Dota 2 all day
date: "2019-08-22"
---

I spent the entire day watching The International 2019, only leaving the house
once at 1700 to get some food at Lawson.

![Home, Ootsuka: chicken peperonchino pasta, which I bought because I spotted
some broccoli. Close but not quite as good as the regular peperonchino.](./IMG_20190822_171727.jpg)
![Watching Mineski get eliminated :(](./IMG_20190822_171948.jpg)
![Ham cheese and lettuce sandwich.](./IMG_20190822_172231.jpg)
![You can never have too much lettuce.](./IMG_20190822_185947.jpg)

I thought that I might be able to go to a bouldering gym later at night since
I'd seen one in Shinjuku that had cheaper entry after 9pm, but the games ended
up finishing at 2200 or so. Elbert went out for dinner by himself since I was
busy watching a game, then brought me back some food from FamilyMart.

![Rice bento. Not bad except for the rice, which I didn't like. Why can't they
just make it as good as the onigiri rice?](./IMG_20190822_234212.jpg)
![Famima Sweets branded pudding. Holy shit this was amazing. The sauce has the
bitterness of caramel without the sweetness, and it balances out the rest of
the pudding perfectly. The pudding itself is incredibly soft and creamy. I tried
eating it with the same fork that I used to eat the bento, but the pudding was
slipping through the prongs.](./IMG_20190823_011709.jpg)
![10/10 would eat again.](./IMG_20190823_012113.jpg)

After watching this tournament I definitely want to wake up earlier so that I
can eat lunch at good sushi places without paying exorbitant amounts, and so
that I can do touristy stuff during the day like walk around gardens and parks.

##### Waking hours

1455–2800
