---
title: Sushiko Honten Ginza
date: "2019-09-26"
---

![Sushiko Honten, Ginza: I arrived just before my booking time at 1130. Lucky
for me, it was easy to find and I made sure to know what the name looked like
in Japanese.](./IMG_20190926_112931.jpg)

When I first came and told them that I had a reservation, one of the staff
welcomed me to the counter on the ground floor, but as I headed to the seat,
someone else directed me upstairs, to what I presume to be the foreigner
counter. The foreign customers that arrived at my counter later provided more
anecdata, along with a Japanese businessman who was hosting a guest.

I went with the 15000 JPY sushi only option rather than the 25000 JPY sushi +
otsumami/sashimi option. I think I made the right decision because I was stuffed
full at the end and I can't say the sushi was worth the price, relative to the
other restaurants that I've eaten at so far. I'd say Sushi Aoki is still the
best that I've had so far, but I know that there's better out there, it just
takes some difficulty to get a reservation or find the right place.

I didn't take photos of most of the pieces because I felt a little intimidated
sitting at the counter by myself (no other guests had arrived yet), although I
did take pictures of some of the more interesting looking ones.

- Ootoro. This was the leanest ootoro that I've had by far, to the point that I
  questioned whether some of it was akami.
- Flounder.
- Hirame.
- Raw prawn. It was still convulsing as the chef shaped it onto the rice.
- Abalone.
- Uni. Served in the regular nigiri style with a little bit of wasabi
  underneath, rather than as a battleship roll. Tasted amazing.
- Squid.
- Striped Jack. It was sort of squishy and chewy, and had loads of flavour.
  Really good.
- Aburi toro. It looked a bit like beef.

![Aji (horse mackerel).](./IMG_20190926_115122.jpg)

- Cold crab meat.

![Shiitake mushroom. It had a nice caramelly smell. Good but I can't say it
was amazing, more just novel and interesting.](./IMG_20190926_115652.jpg)

- Anago (sea eel). It was so hot that I couldn't put it in my mouth, and when I
  did I had to let it sit there with an open mouth for a while looking like an
  idiot before I started chewing it and tasting it. Delicious otherwise.

![Cucumber and sesame seeds roll (after eating two), toro roll (after eating
two), tamago. The tuna roll surprised me with how good it was, I guess the
tuna was quite fatty. The egg was served at room temperature and had this
creaminess to it that I enjoyed. I rushed while eating the hand rolls without
realising it, and the chef told me that I could relax and take my
time.](./IMG_20190926_120807.jpg)

The chef was friendly although he only spoke a little bit of English. Since my
Japanese is getting better we actually managed to make a bit of small talk, but
when I asked him for food recommendations he ended up telling me what food he
liked instead, like kaiseki in Kyoto and tempura in Tokyo. He also told me that
this shop hosts a lot of foreigners, especially from Hong Kong.

Since I was already in Ginza, I spent my afternoon walking around there, as
usual. My walking took me past the metro entrance where Sukiyabashi Jiro is
located so I popped in to have a look at the shopfront. I considered peeking in
to try to book in person even though I had a 0.01% chance of success, but
decided against it in case customers were mid meal and I disturbed their
heavenly experience.

![Tokyu Hands, Ginza: one rack out of maybe ten dedicated to pens, and if you
include pencils, markers, and highlighters, there'd probably be 15-20 in
total. And that doesn't include the luxury pen
section.](./IMG_20190926_131655.jpg)
![These Jetstream displays feature prominently in most stationery sections
that I've been in. I wonder if it's a recent and temporary marketing
campaign.](./IMG_20190926_132018.jpg)
![Half hour complimentary engraving on these mid-range 1500-5000 JPY
pens.](./IMG_20190926_132453.jpg)

I don't know how I managed it, but I spent more than four hours on stationery
floor. I got caught up sampling most of the pens and trying to find the perfect
notebook before I settled on something to buy. By the time I got out it was dark
and so I didn't end up going to Hamarikyu Gardens, something which I'd planned
to do.

![Ginza: looks great at night.](./IMG_20190926_180240.jpg)
![Toraya, Ginza: I ordered a tea and sweets set here. The 1350 JPY price tag
made sense once I realised that I had three different amazing teas on top of
the food. Perhaps it was the combination with the sweets, but it was the best
bunch of teas that I've had so far.](./IMG_20190926_181941.jpg)
![Roasted tea with free refills, served cold.](./IMG_20190926_181859.jpg)
![Yokan with sandwiched red bean paste, apparently a seasonal special based
on autumn colours. The matcha, already amazing in its own right, matched the
red bean perfectly. The food was good but nothing amazing, maybe that's just
the ceiling for this type of dessert.](./IMG_20190926_182153.jpg)
![Then I was surprised when they served me this hot green tea afterwards.](./IMG_20190926_183310.jpg)

I spent a little while in a toy shop, which one can also easily lose all of
their time in, especially at the Hanayama puzzle section.

![ ](./VID_20190926_191502x264.mp4.gif)
![Interesting pen click mechanism.](./VID_20190926_191806x264.mp4.gif)
![The aim of this coordination toy is to line up all of the blocks vertically.](./VID_20190926_192225x264.mp4.gif)
![Hahuhinkan Toy Park, Ginza: plenty of things for Ghibli fans.](./IMG_20190926_193052.jpg)
![The camera doesn't capture how much depth there is in these cardboard kits.
That power pole is 3D, not part of the
background.](./IMG_20190926_193123.jpg)
![ ](./VID_20190926_193211x264.mp4.gif) ![ ](./VID_20190926_193420x264.mp4.gif)
![Feels sort of weird holding a shirtless man.](./IMG_20190926_193543.jpg)
![Nice 150 piece puzzles.](./IMG_20190926_194619.jpg)
![I wish I got to see this, but it was closing time.](./IMG_20190926_200039.jpg)

I had dinner at an izakaya after another long day of shopping. I must've looked
bored because the Vietnamese waitress struck up a conversation with me. When she
learnt that I was from Sydney she said that her 29 year old sister was studying
in Sydney. It turned out that we were both the same age and that she's been
working in Japan for three years.

![Sakanagashi bar, Shimbashi: dinner at an izakaya. This was a lemon sour.](./IMG_20190926_201612.jpg)
![I liked the prawn but the pasta itself was average.](./IMG_20190926_202752.jpg)

I got home at 2200 and looked up places to go for my last few weeks here in
Tokyo, as well as what to do for my upcoming trip to Hokkaido. When I went to
bed I proudly said to Elbert how I'd been saying to people (in Japanese) that
I'm on holiday because I've finished uni, but he told me that I've actually been
saying that I studied at uni. Which means that I've been sounding like an ass
randomly saying that I studied at university, rather than that I _finished_
studying at university.

![Home, Ootsuka: my hall from Tokyu Hands. I didn't buy a refillable notebook
because the one that I saw last time at Shinjuku wasn't there, although I did
buy a book just to practise writing Japanese with my new
pen.](./IMG_20190926_234535.jpg)
![Since so many pens at the stationery sections are made in
Japan, and this one costs 300 JPY, I forgot to check where it was
made.](./IMG_20190926_234118.jpg)
![It's sneakily written under the part of the label that's hidden underneath
the clip.](./IMG_20190926_234136.jpg)
![ ](./IMG_20190926_234234.jpg)
![It extends like this. There was a sturdier, thicker, Platinum one which
extends a little longer, but they were 1000 JPY each and less suitable on a
little notepad spiral.](./IMG_20190926_234244.jpg)
![Learning to write a few hiragana.](./IMG_20190927_021028.jpg)

![ ](./IMG_20190927_031655.jpg)
![Interesting texture. I liked it.](./IMG_20190927_031734.jpg)
![ ](./IMG_20190927_034132.jpg)
![This was 170 JPY or so, quite a bit more than the usual 110-130 JPY.](./IMG_20190927_034512.jpg)
![I think it was steamed egg, rather than the regular milk pudding. I liked
it, but nothing compares to the melty, creamy, mouth coating goodness of
those Ohayo puddings.](./IMG_20190927_034822.jpg)

##### Waking hours

1030–2835
