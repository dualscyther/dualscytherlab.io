---
title: Exploring Roppongi Hills
date: "2019-08-19"
---

We'd decided to go to Roppongi Hills the night before, but we ended up sleeping
late so rather than getting there at noon, we got there at 1700. We were near
where we thought the art gallery was so we looked around for it, but turns out
it's at the upper levels which requires lining up to buy a ticket to go to the
observation deck. I considered, but it wasn't a particularly good day for it and
I'd already been to the Metropolitan Government building in Shinjuku.

![Roppongi Hills, Roppongi: standing at the edge of the complex. The 238m Mori
Tower is pretty awe inspiring, it completely dwarfs everything around it.](./IMG_20190819_164430.jpg)
![The West Walk, Hill Side and Metro Hat are where the shops are so that's where
we went today. The main tower is an office building (I think this is where
Google's main Japan office is).](./IMG_20190819_164503.jpg)
![A giant spider!](./IMG_20190819_164808.jpg)
![I liked this entrance into Mori Tower.](./IMG_20190819_164822.jpg)
![Some Doraemon exhibit in the plaza outside.](./IMG_20190819_164912.jpg)
![When will Maccas do nitro coffee in Sydney?](./IMG_20190819_170208.jpg)
![A very expensive cup at the art/souvenir store outside the lifts to the
observation deck.](./IMG_20190819_171314.jpg)

I was super hungry so we went up to the restaurant level at the West Walk. It
was quite empty since the dinner period was just starting. Most of the
restaurants were at least 5000 JPY, although we ended up finding an Italian bar,
which I noticed served food similar to what you'd find at a pub/bar in Sydney.
We could see into the kitchen so I also noticed that most of the kitchen staff
were from subcontinental or Southeast Asia.

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190819_175941.mp4" type="video/mp4">
  </video>
  <figcaption>
    Rigoletto Bar and Grill, Roppongi Hills: octopus to start.
  </figcaption>
</figure>

![Squid ink taglioni. Texture was great and the prawns were sweet and fresh. A
bit too oily.](./IMG_20190819_180459.jpg)

After eating, we found a cafe at the West Walk and I had a coffee while Elbert
bought a milk tea to have as we were walking. During our window shopping, we
walked into an expensive Japanese leather shoe store and I liked some of the
shoes that I saw. I asked one of the staff if I could take a photo and he said
"dame desu ne" which translates to "no good", but he responded so quickly and
matter-of-fact-ly (and I didn't understand "dame" in the first place) that I
didn't realise that he'd actually said no, so Elbert had to translate as I was
about to take a picture. After that, Elbert was also told that he wasn't allowed
to drink inside the store, which was perfectly reasonable but we found it
strange that he wasn't told as soon as we'd entered. Perhaps the staff hadn't
wanted to ruin a sale.

![West Walk, Roppongi Hills: looking down into the West Walk. This picture makes
it look like any shopping center here, but it doesn't really capture the
ambiance of the wide open spaces lighting, and expensive shopping.](./IMG_20190819_183933.jpg)
![There was some sort of Pixar thing going on at the time. Buzz is on the right.
A lot of shops had Pixar characters sitting out the front.](./IMG_20190819_184249.jpg)
![Roasted Coffee, Roppongi Hills: a little cafe inside the West Walk. It
would've been interesting to say that I ate "Roasted Dog" in Asia.](./IMG_20190819_184530.jpg)
![I was hopeful since the menu had quite a bit of coffee on it, but the espresso
was average (as in average, not as in bad).](./IMG_20190819_185034.jpg)

There was also a specialty store with many different types of Japanese made
socks. They all felt very nice with quite dense fabric, so I was tempted to buy
some of the more colourful or interesting ones. However, they all had "錦"
(nishiki) as their primary material, and a quick search was only able to
translate it to some material called "brocade" which I had no idea about. Later
that night we looked up the translation for cotton and realised that it also
translates to "nishiki".

![Tabio, Roppongi Hills: some socks that I maaaaybe should've bought.](./IMG_20190819_191206.jpg)

After our window shopping we took a walk through Mohri Garden which on our guide
brochure was supposed to be quite nice, but found that a bar and outdoor area
had replaced the large pond in the middle. There were plenty of young office
workers there having after work drinks, but not much garden to see. We also
walked to Sakurazaka Garden on the other side of the complex but it ended up
just being an unlit (it was night by then), small playground.

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190819_195757.mp4" type="video/mp4">
  </video>
  <figcaption>
    Sakurazaka Garden, Roppongi Hills: it wasn't really a garden so much as just
    a small playground, but at least I'd never slid down a slide like this
    before.
  </figcaption>
</figure>

Since Roppongi is famous for its nightlife, we decided to walk around the
district just to see what it had to offer and to soak in the ambiance; as we
were doing so, we caught a glimpse of Tokyo Tower in the distance so we walked
the fifteen minutes to check it out.

![Shake Shack, Roppongi Hills: I was hungry and thirsty so this was the perfect
thing to walk past.](./IMG_20190819_201553.jpg)
![Black sesame "large" shack. Japan exclusive, but probably more due to the fact
that they don't think it'll take off outside of Japan.](./IMG_20190819_202747.jpg)
![Thick and tasty. Made me super thirsty afterwards.](./IMG_20190819_202848.jpg)
![Roppongi: spotted this on the walk to Tokyo Tower.](./IMG_20190819_212357.jpg)

Tokyo Tower was a lot taller and wider than I had pictured in my head (not that
I'd ever consciously pictured it or anything), and it was really well lit. I
actually appreciated looking at it more than I thought I would. We took some
pictures outside, and also walked in to see what was on offer at the (pretty
boring) souvenir store.

![Tokyo Tower: I don't really know what they were going for here, it doesn't
seem consistent with their branding and it doesn't look particularly good. I
took a photo anyway.](./IMG_20190819_213951.jpg)
![Interesting decision to have the main text in English.](./IMG_20190819_214106.jpg)
![The view from under the tower.](./IMG_20190819_214023.jpg)
![The view from next to the tower. The way it points into the sky, its bare
structure, and the way it illuminated the clouds, reminded me of some sort of
sci-fi terraforming device.](./IMG_20190819_220401.jpg)
![The view from a bit further away (very original captions).](./IMG_20190819_220747.jpg)

We caught the subway to Sugamo afterwards since Tokyo Tower's nearest train
station (Onarimon) wasn't on the JR Yamanote line. Elbert dragged me into the
local Namco arcade and we spent some time there. I watched him play a rhythm
game and we also watched some people playing Tekken. Elbert said that this
particular arcade was a known hotspot for serious Tekken players. I also
increased my risk of lung cancer with the amount that I passive smoked in there.

<figure className="blogpost-video">
  <video controls >
    <source src="egg-hands.mp4" type="video/mp4">
  </video>
  <figcaption>
    Namco, Sugamo: Elbert says he's rusty but this still looked crazy. I had a
    go at playing this too; it hurts your fingers slamming them down.
  </figcaption>
</figure>

We got home at around midnight and I don't really know where the time went since
I definitely didn't spend most of it on my blog.

![Home, Ootsuka: sweet, juicy, firm. I had to buy these when I saw them at
Lawson since I hadn't really had fruit properly in ages. Absolutely delicious,
but 257 JPY.](./IMG_20190819_235340.jpg)
![I don't know why I got a large can, but I liked it.](./IMG_20190820_012605.jpg)
![This always sits next to the ice cream daifuku at Lawson so I decided to give
it a try.](./IMG_20190820_041149.jpg)
![The packaging was cute. The amount of packaging was also a bit excessive but
that seems to be the story with most things here.](./IMG_20190820_041156.jpg)
![Yum! Ice cream wrapped in a very thin layer of chocolate. I don't really like
chocolate coating ice cream so I was glad that it was thin.](./IMG_20190820_041422.jpg)

I did spend some time lowering the resolution on my uploaded photos (so now
they're ~2MB rather than ~10MB) since otherwise I'm going to hit the 10GB limit
on GitLab repositories pretty quickly. This post is already 50MB without the
videos. This means that if every post were this size, I could store 200 of them,
so I'll need to figure out an alternative solution at some point (self hosting
would be ideal).

##### Waking hours

1430–3030
