---
title: Manten Sushi Marunouchi
date: "2019-09-24"
---

I didn't get enough sleep but that didn't stop me from getting up early so that
I could make it to Manten Sushi, a place that Bob's aunt had recommended to me,
where walk-ins are 3000 JPY (plus tax) rather than 6000 JPY, and even at 6000
JPY it's meant to be a steal.

![Lawson, Ootsuka: a rice ball to expand the stomach. The range on the fridge
was much larger than I was used to, probably because I've never really been
there in the mornings.](./IMG_20190924_095212.jpg)

The author of a random blog that I read said that he got to Manten Sushi at 1030
to wait for its 1100 opening, so I followed in his footsteps and got to Tokyo
station at 1020. Marunouchi is a big commercial district but the streets were
quiet, perhaps everyone was busy being in the office. Walking around reminded me
of the nicer areas of Manhattan.

![Naka-dori Street, Marunouchi.](./IMG_20190924_102600.jpg)
![Marunouchi Brick Square, Marunouchi: the restaurant was on the basement level.](./IMG_20190924_102607.jpg)

I was the first one there. I could see staff inside preparing for service, but I
wasn't sure whether I was meant to let them know that I was waiting or whether I
should not bother them, so I just took a seat on one of the queueing chairs
outside. About five minutes later two foreigners came by and saw that I was the
only one waiting, so they presumably left to walk around for a bit before coming
back. That was their mistake, since along came a middle-aged/older Japanese lady
who asked me something probably related to whether I was queueing. I couldn't
understand her so she just went to the shop and asked some questions. A waiter
came out and asked if I had a reservation, then told me and the old lady to take
a seat and wait. A friend of hers came along to join us in the queue, and we
made some conversation in broken English/Japanese about how I knew about this
place, what I was doing in Japan, how long I was here, where I was from, etc.
The foreigners returned and were turned away because there were only three
non-reservation seats available that day.

The food was great, and it's definitely really good value for the price. My chef
served the same meal to me and the Japanese businessman next to me, but I
noticed that everyone else at the counter (and at the few tables) had different
food. For example, one group had some sashimi served to them, and another pair
to my left had otsumami and aburi toro to start rather than yellowtail. I'm not
sure whether that's just the norm, whether it's because I had the walk-in 3000
JPY special, or whether it's because I wasn't a regular. It was good that the
person next to me ate the exact same pieces at the same time as me, because the
chef gave more specific introductions to the neta in Japanese, e.g. saying that
something was from Hokkaido rather than the simple English name of the fish.

![Manten Sushi, Marunouchi: a strange soup to start that had a dry taste.](./IMG_20190924_110221.jpg)
![And some seaweed.](./IMG_20190924_110527.jpg)
![Aburi Hokkaido yellowtail. Tasted absolutely amazing.](./IMG_20190924_110840.jpg)
![Hokkaido octopus. This was the best octopus I've ever had. It had some
flavour, and it wasn't as chewy as I'm used to.](./IMG_20190924_111133.jpg)
![Saba (mackerel). Not as good as Sushi Aoki but almost.](./IMG_20190924_111316.jpg)
![Enoki mushrooms. Good but meh relatively speaking.](./IMG_20190924_111503.jpg)
![Yuba tofu (introduced as Japan tofu). Incredibly creamy and melty, yet had
this nice starchy lingering taste that coated your
mouth.](./IMG_20190924_111605.jpg)
![Abalone. I had to eat it in three separate bites. Not that noteable in my
opinion.](./IMG_20190924_111819.jpg)
![Squid or cuttlefish, but I'm leaning towards cuttlefish. A bit too
chewy.](./IMG_20190924_112041.jpg)
![Peanuts. As good as peanuts can be I suppose. It provided a good break from
the eating since the pace had been quick.](./IMG_20190924_112158.jpg)
![Akami. I don't think the sesame added much, but the piece was good
nonetheless.](./IMG_20190924_112409.jpg)
![Ootoro. Very fatty, still had a tuna taste but wasn't as strong as usual so
the less vinegared rice suited it perfectly.](./IMG_20190924_112534.jpg)
![Salmon roe on rice. Maybe it was how it was served, or maybe it was the
salmon roe, but this might've been the best I've
had.](./IMG_20190924_112748.jpg)
![Hokkaido uni. Amazing but I was so full and the pieces were coming so
quickly that I don't think that I appreciated it as
much.](./IMG_20190924_113159.jpg)
![Tamago. I liked that it was a small piece so the sweetness was fine,
otherwise it would probably be a bit too sweet.](./IMG_20190924_113356.jpg)
![The miso soup came towards the end. I ate a few clams then I gave
up.](./IMG_20190924_113541.jpg)
![Anago (sea eel). Not as melty as I hoped but the flavour was there.](./IMG_20190924_113634.jpg)
![Negi toro (onion and tuna) hand roll. The tuna was great but overall it was
eh since I was really full and there was so much
rice!](./IMG_20190924_113855.jpg)
![Kampyou, pickled and dried gourd stips or something. It was okay.](./IMG_20190924_114031.jpg)
![Some amazing grapes which were introduced as dessert. The guy next to me
left the skins for some reason, maybe that's the norm when eating big grapes
here?](./IMG_20190924_114919.jpg)
![ ](./IMG_20190924_114801.jpg) ![ ](./IMG_20190924_115945.jpg)

I noticed that the rice was quite brown from whatever they mixed it with. It
also wasn't very vinegared but it ended up balancing well with the majority of
the pieces. The chef made sure to wipe our plates after almost every piece,
whenever he saw a drop of sauce left behind.

Afterwards I browsed some of the shops outside and walked around Marunouchi. I
spent about half an hour in a Herman Miller showroom trying out chairs and
talking to one of the salespeople. Her English was quite good and she could
explain everything, although sometimes she'd use the wrong word like "it is very
weigh".

![Herman Miller, Marunouchi: surprisingly comfortable.](./IMG_20190924_122330.jpg)
![Naka-dori Street, Marunouchi.](./IMG_20190924_121738.jpg)
![ ](./IMG_20190924_121755.jpg)
![Some rugby world cup decorations.](./IMG_20190924_130053.jpg)
![ ](./IMG_20190924_130618.jpg) ![ ](./IMG_20190924_130625.jpg)
![They close the street in the middle of the day. This street was lovely,
along with the rest of Marunouchi.](./IMG_20190924_130812.jpg)
![World cup stuff.](./IMG_20190924_130914.jpg) ![ ](./IMG_20190924_131010.jpg)
![They even have a shrine.](./IMG_20190924_131033.jpg)
![Another live viewing area with a handy timetable.](./IMG_20190924_131231.jpg)
![Currently streaming a live concert.](./IMG_20190924_131312.jpg)
![I don't know if that's the real trophy. You can take a photo with it and
pose with the balls.](./IMG_20190924_131908.jpg)
![I might've thought about collecting all the stamps if I wasn't going the
opposite way.](./IMG_20190924_131750.jpg)
![ ](./IMG_20190924_131802.jpg)

I walked back through Tokyo station, towards Nihombashi.

![Tokyo station, Marunouchi.](./IMG_20190924_132608.jpg)
![ ](./IMG_20190924_132719.jpg)
![Inside the north west entrance.](./IMG_20190924_133331.jpg)

I was going to skip Mitsukoshi since I'd done enough walking around in
department stores for a lifetime, but I gave it a spin since it's the oldest
department store in Japan and it looked fancy from the outside.

![Mitsukoshi annex, Nihombashi: a little chair to sit on in the
elevator.](./IMG_20190924_143928.jpg)

![Mitsukoshi, Nihombashi: that's an elevator door.](./IMG_20190924_144412.jpg)
![ ](./IMG_20190924_144458.jpg) ![ ](./IMG_20190924_144617.jpg)
![ ](./IMG_20190924_145835.jpg)
![Half of one of the (very large) floors was dedicated to selling kimonos,
the original thing that Mitsukoshi sold. All the staff in this area were
wearing kimonos.](./IMG_20190924_151626.jpg)
![ ](./IMG_20190924_151944.jpg) ![ ](./IMG_20190924_155356.jpg)

I went back to Tsuruya Yoshinobu for the confectionary demonstration. It was
somewhat expensive but that seems to be the going rate for these "sweets with
matcha tea" meals. I didn't take a picture of it but the green tea that I was
initially served was great.

![Tsuruya Yoshinobu, Nihombashi.](./IMG_20190924_160449.jpg)
![They're all essentially the same except filled with slightly different
pastes.](./IMG_20190924_155845.jpg)
![Maybe I should've got the middle flower, it seems like it'd take a bit more
effort to make. My one was still not bad though.](./IMG_20190924_160035.jpg)
![ ](./IMG_20190924_160119.jpg)
![It tasted as good as red bean filling can taste. Decent but not great, but
goes incredibly well with matcha tea to even out the
bitterness.](./IMG_20190924_160202.jpg)
![ ](./IMG_20190924_160327.jpg)
![Roasted tea to finish. It was incredibly roasted, almost burnt tasting to
the point that it was very slightly unpleasant. I've never tasted any tea
before quite like it.](./IMG_20190924_160550.jpg)

After some sock shopping at Tabio, I walked one suburb over to Kanda, where
there was a Michelin starred udon shop that I wanted to try. I walked around the
block and couldn't find it, but eventually saw a sign in front of some
construction fencing, saying that it was closed. It was a bit strange because
there were Google reviews of it posted today, but I guess that the review
doesn't need to be done on the day of the meal. I desperately needed to find a
bathroom so I went into the nearest coffee shop.

![Ammonite Coffee Market, Kanda: a double espresso with medium roasted Brazil
beans. It was brewed a bit too hot for my liking, but was otherwise not that
bad.](./IMG_20190924_185227.jpg)

I decided to have dinner near home instead.

![Eightdays Cafe, Ootsuka: all you can eat Italian ham. I wasn't trying to
stuff myself so I only ate four plates. It was good but not amazing, but I
think it's to be expected since you can't eat too many slices without feeling
salted up.](./IMG_20190924_201039.jpg)
![Not a fan of this hot wine.](./IMG_20190924_202553.jpg)

Elbert and I went to the conbini after I showered, had a bit of a nap, and
learnt some Japanese.

![Home, Ootsuka: finally finished the bag of these.](./IMG_20190924_232910.jpg)
![Some almond praline chocolate thing which I bought purely because of the
Ohayo brand.](./IMG_20190925_022449.jpg)
![Tasty and creamy, but less creamy and mouth coating as the regular Ohayo
milk puddings.](./IMG_20190925_022636.jpg)

##### Waking hours

0900–2820
