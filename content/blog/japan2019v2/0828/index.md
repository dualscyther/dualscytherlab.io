---
title: Sushi Kidoguchi
date: "2019-08-28"
---

I didn't get much sleep: I got to bed late, couldn't get to sleep, and woke up
briefly at 0830. What a regret booking so early for 1200. I still managed to
leave the house before 1100, giving myself plenty of time to get to the
restaurant. I ended up with fifteen minutes to spare so I stood outside
Omotesando station under some cover from the light rain which my weather app had
failed to warn me about. In that time I witnessed a woman drop her sesame
tea/coffee/smoothie drink right at the top of an escalator; it exploded onto one
or two other people and she cleaned herself up then walked off, leaving the
evidence behind at the crime scene.

![Harajuku JR station: expanding the stomach with a milk donut.](./IMG_20190828_112731.jpg)

I'd been to Sushi Kidoguchi in February on Jack's recommendation, so I knew what
I was getting myself into. Great sushi at an affordable price, with a friendly
chef that could speak fluent English and even some Cantonese phrases when he
felt like being funny. We made casual conversation and he introduced the name of
each piece in English. He asked me to jog his memory about when I was last here
and I said that we came as a group of four and one of us spilled miso soup
everywhere.

The shari (rice) had a good balance and strength of sweetness and sourness, and
it was served nice and warm. It's my favourite shari that I've had so far.

![Sushi Kidoguchi, Omotesando: striped jack (shima-aji) and chuutoro. So much
flavour out of both of these and they went really well with the rice.](./IMG_20190828_120255.jpg)

I forgot to take a picture of the squid with salt and lemon juice. It was
slightly chewy, but pleasantly so.

![Tuna back (lean tuna) with yuzu zest. Lots of flavour, quite soft, and
fragrant due to the zest.](./IMG_20190828_120618.jpg)
![Miso soup that I ended up spilling half of. Also note the hot hand wrung
towel.](./IMG_20190828_120744.jpg)
![The salmon roe had heaps of flavour but wasn't fishy. The uni was very sweet
and creamy. Not no. 1, but amazing nonetheless.](./IMG_20190828_120903.jpg)
![Sea eel. Nice caramelised bits that had a very slighty crunch, sweet but not
overly so.](./IMG_20190828_121303.jpg)
![Snow crab, which the chef passed straight to my hand. The crab was quite hot,
so it was an interesting sensation having the neta warmer than the shari.](./IMG_20190828_121602.jpg)
![Rolls and tamago. The rolls were good but nothing special. I didn't write down
any notes about the egg but I remember liking it.](./IMG_20190828_121745.jpg)
![I asked if I could take a picture so he picked up his wasabi stick to pose.
I believe that's the owner-chef on the right laughing at him.](./IMG_20190828_122053.jpg)
![Striped jack again, this time cooked and passed straight to my hand. It was
awkward taking a picture of this.](./IMG_20190828_122608.jpg)

Towards the end of my meal I accidentally knocked the miso soup bowl over,
spilling it all over the counter, my t-shirt, and my shorts. I asked the chef
how often customers spill the miso soup; he laughed and said that it's mainly
drinks at night, but almost never the miso soup. Some tasty pickles finished off
the meal, but I forgot to take a picture since I was worrying about my
miso-smelling shorts.

It turns out that the chef is in his late 30's, and he studied in Canada for
many years at the age of 11, which explains his fluent English. I forgot to ask
for his name, but I will next time. I asked if he ever had thoughts in English
while he was in Canada and he said that it took him about five years for him to
start having English dreams and sleep-talking.

The whole experience took a short forty minutes, and for 3240 JPY it's entirely
worth it. I was also recommended to go to a Sushi Midori in Ikebukuro so I might
give that a try in the coming weeks.

Feeling sleeping after my meal (in no small part due to the lack of sleep last
night) I decided to go home via Shibuya to shop at the Mega Donquijote, but once
I got to Shibuya station and saw that it was raining, I couldn't be bothered to
walk the five minutes. Instead, I just went back to Ootsuka, fruitlessly browsed
the station shopping center for sleep masks and found nothing, then went home
and napped for three hours until 1700.

Elbert and I went out to eat his first meal at 2000, then to Daiso for a
sleeping mask, then to the supermarket to stock up on some food.

![Hopeken, Ootsuka: chuukasoba, as tasty as the day that I first got here.](./IMG_20190828_201122.jpg)
![Home, Ootsuka: convex shape so that your eyes don't get pressured and it
doesn't ruin your makeup.](./IMG_20190828_220730.jpg)
![Tomatoes, carrots, and a thick fruit/veggie smoothie. How healthy.](./IMG_20190828_222537.jpg)
![Trying my mask on for an hour nap.](./danosleeping.jpeg)
![Found this larger pack at the supermarket! I ate an entire row already.](./IMG_20190829_010633.jpg)
![Also ate this carrot raw. Not particularly tasty so I'll probably cook it next
time.](./IMG_20190829_022027.jpg)

Another long blog post meant that I took 3.5 hours to finish it before I went to
sleep.

##### Waking hours

1030–2905
