---
title: Hakodate day 1
date: "2019-10-02"
---

I left house at 0606, got 2 rice balls from Lawson and ran a tiny bit of the way
to make the 0613 train from Ootsuka.

![Ueno station: the guy in front had the same idea as me.](./IMG_20191002_063542.jpg)

My shinkansen was actually two separate trains temporarily joined while in the
Tokyo urban area, but which would split off later. Pretty cool. I also like that
they play music a couple of minutes before every stop so that you can wake up/be
prepared.

![Shin-Hakodate-Hokuto station: I'm in Hokkaido! It was cool outside. The
four hour train ride felt like it only took an
hour.](./IMG_20191002_105906.jpg)
![Shin-Hakodate is in the middle of nowhere so you have to catch a half hour
train to get to Hakodate.](./IMG_20191002_112758.jpg)
![Hakodate station.](./IMG_20191002_112934.jpg) ![ ](./IMG_20191002_113004.jpg)
![A cute display at the station, with the appropriate trains.](./IMG_20191002_113102.jpg)
![I didn't feel like eating my onigiri so I had them for lunch at the station.](./IMG_20191002_114255.jpg)

The plan today was to walk around Hakodate and see things. It was a 2km walk to
my hostel, with many stops along the way.

![Hakodate Morning Market, Hakodate: such a fan of having maps everywhere.](./IMG_20191002_120419.jpg)
![Live squid.](./IMG_20191002_120717.jpg)
![And scallops (I think).](./IMG_20191002_120817.jpg)
![You can fan yourself with money.](./IMG_20191002_122122.jpg)
![ ](./IMG_20191002_122218.jpg)
![Squid-ink soft serve. Tasty as well as creamy, but melting, and it wasn't
even a hot day.](./IMG_20191002_124137.jpg)
![A melon whose name I think was "katta". Tasty, supple and soft. You could
get the same quality in Sydney so I'm not sure that it was worth the 250 JPY
but I had to try.](./IMG_20191002_125933.jpg)

![Kanemori Red Brick Warehouse, Hakodate: these old warehouses have been
turned into shops and restaurants.](./IMG_20191002_132739.jpg)
![ ](./IMG_20191002_132828.jpg) ![Cool glass things.](./IMG_20191002_133739.jpg)
![ ](./IMG_20191002_134158.jpg) ![ ](./IMG_20191002_134938.jpg)
![If only I thought that chopstick rests were useful.](./IMG_20191002_135127.jpg)
![This chopstick store had a huge selection of chopsticks at all price
points.](./IMG_20191002_135143.jpg)
![A mirror-holder with mirror.](./IMG_20191002_135514.jpg)
![Cute woven (woven? I don't know my textiles terms) stuff.](./IMG_20191002_135710.jpg)

After browsing some of the shops I felt pretty dead since I barely slept last
night or on the shinkansen and it was 1400. I sat down for a break and ended up
falling asleep for a good 10-15 minutes. The hostel was next door so I went
there to hopefully check in half an hour early and get some sleep, but I was
only allowed to drop off my bag. That probably ended up being a good thing
because I went back out and walked some more.

![The red circle on the left corner of the bay was where this sign was. My
hostel was Hakoba on the left of that.](./IMG_20191002_143445.jpg)

![The streets all slope up to the base of Mount Hakodate, some quite
steeply.](./IMG_20191002_144149.jpg)
![I just thought that this looked interesting.](./IMG_20191002_144452.jpg)
![Midori no Shima, Hakodate: a little island in the middle of the
bay.](./IMG_20191002_144834.jpg)
![There were some people fishing a bit further down.](./IMG_20191002_145636.jpg)
![Looking back at Mount Hakodate from the island.](./IMG_20191002_150439.jpg)

There were so many temples in the area, it was ridiculous. They're all unique
but they still end up looking and feeling the same after a while. I wasn't even
looking for them, but I felt obligated to go inside every one that I walked
past. I don't blame you for scrolling through.

![ ](./IMG_20191002_152443.jpg) ![ ](./IMG_20191002_152514.jpg)
![ ](./IMG_20191002_153825.jpg) ![ ](./IMG_20191002_153902.jpg)
![ ](./IMG_20191002_154038.jpg)
![I would've thought that it might be considered disrespectful to have cars
parked inside the grounds or something but I guess
not.](./IMG_20191002_154115.jpg)
![ ](./IMG_20191002_154135.jpg) ![So much detail.](./IMG_20191002_154651.jpg)
![ ](./IMG_20191002_155207.jpg)

![Tea Shop Yuhi, Hakodate: It was a decent walk to get here and there's not
much else except the nice view of Hakodate Bay and great tea. This was just
the tea that they give you when you come in, but it was
delicious.](./IMG_20191002_161112.jpg)
![I ordered matcha mixed with milk because I had no idea what it would taste
like. I wouldn't travel just for this but it was good. The bubbles had an
amazing milk smell. Since the sun was setting, it was in my eyes while I was
having this, which was a little annoying.](./IMG_20191002_161718.jpg)
![I chatted to a middle aged Japanese guy with a huge camera about how it was
a nice view with nice colours. As well as my Japanese would allow
anyway.](./IMG_20191002_170358.jpg)
![ ](./IMG_20191002_170919.jpg)

It was the walk back that really killed me with the temples. It was cool how a
lot of the sights had signs with history written on them.

![What are those bibs?](./IMG_20191002_171757.jpg)
![ ](./IMG_20191002_172019.jpg) ![ ](./IMG_20191002_172049.jpg)
![ ](./IMG_20191002_172147.jpg) ![ ](./IMG_20191002_172157.jpg)
![ ](./IMG_20191002_172218.jpg) ![ ](./IMG_20191002_173043.jpg)
![ ](./IMG_20191002_173054.jpg) ![ ](./IMG_20191002_173149.jpg)
![ ](./IMG_20191002_173522.jpg) ![ ](./IMG_20191002_173833.jpg)
![ ](./IMG_20191002_173900.jpg) ![ ](./IMG_20191002_174916.jpg)

When I was checking in I managed to get through most of the process speaking
Japanese (there wasn't much speaking to be done after all) but the receptionist
switched to English when she realised that I had no idea what she was saying as
she explained the rooms and procedures.

![Share Hotels Hakoba, Hakodate: what a great hostel. The beds were spacious,
sturdy, and private, and the aisles were so wide!](./IMG_20191002_181953.jpg)
![You get your own light and privacy curtain.](./IMG_20191002_181959.jpg)
![Space to hang your stuff.](./IMG_20191002_182002.jpg)
![And a locker.](./IMG_20191002_182007.jpg)
![Nice provided cardholder.](./IMG_20191002_182743.jpg)
![I don't know about other rooms, but my room was huge and probably had
thirty beds. This is a sort of creepy view looking in from the upstairs
playroom.](./IMG_20191002_195916.jpg)
![The other areas were meh but no one really cares if the actual dorm is
amazing. I suppose it was more like a capsule hotel, including supplied
toiletries.](./IMG_20191002_195921.jpg)
![ ](./IMG_20191002_200001.jpg)

I lay in bed for a while, napped, and showered. Sadly they didn't provide
moisturiser which is one thing that I always can't be bothered bringing but
would also love to have.

I walked around town to find dinner and noticed that it was quite dead. Many
restaurants were closed even at 2100, and there was barely anyone on the
streets. I guess I'm just used to Tokyo. I wore pants (sweat pants) for the
first time this trip since it was so cool.

![Grano E Acqua, Hakodate: salmon cream and some Japanese fruit pasta. Good
at first and then got a bit too creamy.](./IMG_20191002_213506.jpg)
![A coloured manhole cover.](./IMG_20191002_221447.jpg)

When I got back to the hostel I blogged while sipping on milk, making me sleep
rather late.

![Desperate to try milk in Hokkaido, I went to the closest 7-11 and bought a
litre since that was the only size that they had. It wasn't even that good
and I felt a bit nauseous from fullness after drinking a quarter of the
carton so I put the rest in the fridge.](./IMG_20191002_223551.jpg)

##### Waking hours

0555–2620
