---
title: Why/how I'm blogging every day
date: "2019-08-13"
---

- I'm blogging for myself, so that I can remember what I did when I look back in
  however many years.
- I'm blogging for others to see what I'm up to. I know that there are social
  media platforms for that, but it's nice to be in control of your own content,
  its medium, and its presentation. You can't be banned, and you can't lose your
  content if the platform shuts down. Sure, most people have their photos saved
  separately from the platform, but do they also save their captions?
- I'm trying to post daily because it's easier to write while recall is easy,
  and a daily, journal style blog is easier than having to think about what I
  did over the last week and trying to summarize it succinctly. Because it's a
  weekly summary, I also feel more obligated to edit my writing so that it flows
  better or even makes any sense at all.
- Writing daily is also an easier habit to form and to continue than remembering
  to write every week.
- I'm using Trello (any note-taking app will do) to write a detailed log of what
  I did every day, as well as jot down ideas for additional content (like this
  currect section!).

## Lazy Day #2

I got up late again at 1400 and bought breakfast from the closest Lawson. When I
got back I asked Elbert to help me book a sushi restaurant for Jack, Cat and I
(my brother and sister-in-law) but unfortunately they were closed for the rest
of the week for a Japanese festival/custom called Obon. I noticed that quite a
few local shops were closed for the same reason too.

![Home, Ootsuka: Egg sandwich, tuna mayonnaise and salmon roe onigiri. Jack said
that Lawson onigiri were the best, so I had to give it a go. Apparently the
salmon roe was the best but I mistakenly bought the one in the premium packaging
rather than the regular line.](./IMG_20190813_153035.jpg)
![An unassuming exterior. Yes I'm eating over the sink.](./IMG_20190813_153105.jpg)
![Oishii.](./IMG_20190813_153224.jpg)
![I have to get this whenever I see it.](./IMG_20190813_160357.jpg)
![One for now and one for later.](./IMG_20190813_160459.jpg)

The rice really did a number on me because I felt so sleepy that I went back to
bed until Jack woke me up at 1800 to go out and eat some dinner. We went to an
izakaya place to start, then headed to Jack's favourite ramen restaurant, Menya
Musashi for the main meal. We witnessed an interesting scene when a customer
queueing in front of us made a decent fuss[^1]. When we arrived and were waiting
in line, all the staff were joking around with each other and doing that thing
at ramen restaurants where they all chant the same thing out loud. After the
scene, the atmosphere felt like it changed, and I don't think I heard them chant
anything jovially for the rest of the time that we were there.

![Kuriya, Shinjuku: izakaya restaurant. Cheers!](./IMG_20190813_191554.jpg)
![Pork belly skewers.](./IMG_20190813_192508.jpg)
![Chicken skewers.](./IMG_20190813_192622.jpg)
![Some eggplant dish.](./IMG_20190813_200156.jpg)
![Shinjuku, somewhere.](./shinjuku-somewhere.jpg)
![Menya Musashi, Shinjuku: a big mixing spoon and a cauldron of ramen broth.](./IMG_20190813_203711.jpg)
![The pork wasn't the usual thinly sliced stuff which was cool.](./IMG_20190813_203938.jpg)
![People waiting in line behind us, probably looking over our shoulders at the
food.](./IMG_20190813_204041.jpg)

When I got home I looked up sushi restaurants to eat at for the next day, and
found Sushi Aoki in Ginza. Elbert helped me to get a booking over the phone,
which I am extra grateful for because he definitely didn't seem that comfortable
doing it, but that probably won't stop me from asking him again 😛.

![Home, Ootsuka: enjoying dessert when I got home.](./IMG_20190814_024159.jpg)

##### Waking hours

1220–2900

[^1]:

  she was lining up in a group of three, so the staff presumably had some plan
  to wait for three adjacent seats to become free. In doing so, they let groups
  of two and one go ahead of her
