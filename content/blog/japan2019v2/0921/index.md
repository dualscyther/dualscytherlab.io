---
title: Rugby World Cup Fanzone
date: "2019-09-21"
---

Last time I was in Nihombashi, it was with Bob's aunt, it was at night, and we
didn't spend much time there. This time I wanted to go during the day but I
ended up leaving the house pretty late at 1510.

![Tokyo station, Yaesu: the east side of Tokyo station.](./IMG_20190921_155150.jpg)
![Concept Labi Tokyo, Yaesu: I didn't go upstairs but it seems that the only
difference to a normal Labi is that the floors look more like the Apple
store.](./IMG_20190921_161249.jpg)

Walk a few minutes east and they begin to call it Nihombashi. Apparently you
pronounce it Nihon-bashi but if you let it flow then it naturally becomes an m
sound.

![Chuo-dori, Nihombashi: the same road as the one which stretches through
Ginza as its main street.](./IMG_20190921_161728.jpg)
![ ](./IMG_20190921_161832.jpg) ![ ](./IMG_20190921_161957.jpg)

![The Coffee Bean & Tea Leaf, Nihombashi: I don't know why this came in a
takeaway cup. I saw other coffees in mugs but maybe those were reserved for
milk coffees while mine was just the default drip
coffee.](./IMG_20190921_162633.jpg)

The cafe was decently full and had regular tables as well as higher benches with
stools. There were lots of people sitting to relax, read, work and study.
Clearly what I read about not being able to loiter at cafes in Tokyo was wrong.

![When this pasta came I thought to myself, I wonder if pasta is good at
cafes here like it is in Sydney? Turns out the answer is yes. I slurped the
first forkful before I realised that I was eating pasta at a cafe rather than
noodles at a ramen restaurant. The cafe was actually a bit loud but the
tables immediately next to me were quiet so I had to resort to the rolling
pasta on a spoon method.](./IMG_20190921_163022.jpg)

![Nishikawa, Nihombashi: Imabari towels are quite a big brand and they sell
them in a lot of places. They're so serious about their towels that they have
towel sommeliers and exams.](./IMG_20190921_173841.jpg)

Sweets omakase for 1400 jpy. Sugoi. Didn't go because short on time.

![Tsuruya Yoshinobu, Nihombashi: the same shop as the one where Bob's aunt
bought us desserts. The traditional Japanese sweets looked amazing but I was
running out of time so I couldn't try them since I had to meet Elbert
elsewhere soon. They have a few tables to sit down and eat, as well as a
counter where you can watch them craft the sweets in front of
you.](./IMG_20190921_175616.jpg)
![Nihombashi: the bridge itself which the district is named after.](./IMG_20190921_182957.jpg)
![ ](./IMG_20190921_183001.jpg)

Since the Rugby World Cup is on, I met up with Elbert to check out a "fanzone",
one of a few which had been set up around Japan for people to spectate games for
free on a big screen.

![Tokyo Sports Square, Marunouchi: plenty of staff inside to shepherd
patrons. Maybe they were volunteers? I'm not really sure where the money
would come from otherwise.](./IMG_20190921_185831.jpg)
![The ground floor where you could drink. Plenty of tourists here.](./IMG_20190921_185957.jpg)
![The South Africa vs New Zealand game was on. I couldn't see anything and
people were noticeably taller here.](./IMG_20190921_190202.jpg)
![It had the vibe of a convention. There was an area selling merchandise in
the lobby.](./IMG_20190921_190446.jpg)
![Upstairs was the family floor. No alcohol allowed. Everyone sitting made it
easier to actually see the screen. The space in the front half was a "no
shoes" area.](./IMG_20190921_190627.jpg)
![Standing and watching at the back, from the side. I couldn't see much if I
sat, and kneeling was uncomfortable on the fake
grass.](./IMG_20190921_191801.jpg)
![That omurice looks pretty cute.](./IMG_20190921_191939.jpg)
![But it wasn't as cool in reality. It also set me back 900 JPY, was small,
and didn't taste that good. I guess event food can be a rip off in Japan
too.](./IMG_20190921_193914.jpg)

At first I thought that all the Japanese were All Blacks fans but when the
Springboks scored I realised that they just loved cheering for everything.

![Renoir Coffee, Ginza: afterwards we popped into a nearby cafe for a snack
and some loitering. My apple banana smoothie was okay. Elbert had a cafe au
lait, probably the first time that he's had a coffee and I
haven't.](./IMG_20190921_210054.jpg)

After getting back to Ootsuka we walked down the main street of restaurants (on
the north side) to have a look at what was open. This was actually the first
time that I'd walked on the street past midnight, and I was surprised by the
number of touts trying to get us into their clubs.

![A ramen restaurant, Ootsuka: yummy.](./IMG_20190922_001158.jpg)
![Not bad.](./IMG_20190922_001246.jpg)
![It's interesting how the eggs are all slightly different from each other at
each restaurant, but it's hard to describe/remember how. Anyway, I love them
all.](./IMG_20190922_002354.jpg)

I noticed that our server spoke Japanese a little funny when she was taking our
order and shouting it to the kitchen. Elbert said that she was Vietnamese and
told me that Ootsuka actually had a reputation (at least amongst his friends)
for having lots of foreigners.

A group of Japanese guys came in after us and finished their meal while we were
still eating. As they left one of them was lying with their face on his crossed
arms on the table, I assume after a good night of drinking. We watched as his
friend had to gently encourage him a few times to get up and leave. I guess some
things are universal.

##### Waking hours

1250–2935
