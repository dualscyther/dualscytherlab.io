---
title: Star struck and bill shocked
date: "2019-08-27"
---

I'd set some alarms for 1200 and 1300 but when they rang I just turned them off
and slept in instead. With my hopes of getting out early and spending the day
outside gone, I packed my laptop and headed to Lattest to get some blogging
done.

To my pleasant surprise, Mizuki from Terrace House was there when I arrived a
bit after 1700. I ordered as usual and sat down with my laptop, thinking to
myself how I should go about asking for a photo, especially since she was on the
clock.

![Lattest, Omotesando: good espresso, good cookie.](./IMG_20190827_172606.jpg)

In hindsight I definitely overthought everything considering that she must get
fans come in and talk to her all the time. In fact as I was working on my blog,
a pair of tourists came in, ordered drinks, and had a friendly chat with her
while the cafe was quiet.

I decided to ask as I returned my plates to the counter. I texted Elbert for
help with how to say something similar to "sorry for disturbing you" and armed
with some new vocabulary I apologised for disturbing her and asked if she was
"Shida-san", to which she cheerfully replied yes. At this point I was so nervous
that I lost my Japanese speaking ability and asked in English if we could get a
photo together and she said, "yeah sure!" in a minimal accent.

At this point, a few new customers had walked in and even though there was
someone else staffing the counter, I felt bad for taking up her time, so I
hurriedly pulled out my phone and we took a picture together. I blurted out "my
girlfriend watches Terrace House", as if I don't watch too 🤦🏻.

Michelle had wanted me to ask Mizuki for a video of her saying hi, but I
couldn't bring myself to ask 😰. I didn't think that I'd be so nervous but I
attribute it to working myself up to the encounter rather than winging it.

![It's Mizuki! I can't be sure, but I think she paused, expecting me to make
conversation afterwards, but I just thanked her and left. Not sure if that was
a little rude since I asked for her picture without introducing myself. Overall
a lot more nerve-racking than I would've expected out of myself.](./IMG_20190827_182618.jpg)

I walked down to Omotesando Hills (a shopping center) afterwards to look for a
particular cafe called "Imperfect", but when I got there I wanted something more
substantial to eat so I walked towards Shibuya to look for food. Along the way I
stopped by Sushi Kidoguchi to make a booking for 1200 lunch tomorrow (to force
myself to get up early), and the chef recognised me as a previous customer,
which was cool.

![Omotesando Hills: the spiral shape works so well, especially since the road
outside is sloped too.](./IMG_20190827_184742.jpg)

I'd previously combed through some online lists of places to eat in Tokyo, and
saved the ones that looked interesting on my map. One that happened to be close
by was Ryukyu Chinese Tama, an Okinawa-Chinese fusion Michelin starred
restaurant. I expected there to be lines outside but when I went to see how long
the queue was before deciding on whether to eat there, there was actually an
available standing seat at the counter[^1].

The atmosphere was really lively, with plenty of shouting/chanting, jokes, and
conversation between chefs and patrons. Something interesting that I noticed was
that customers were greeted with "konbanwa" rather than the typical
"irasshaimase". It seemed like each chef was in charge of cooking a few dishes
for the restaurant, while also being the server for the customers directly in
front of them at the bar. While I'm sure that every restaurant is organised
chaos, here it was exposed directly for the customer to see and it was awesome
to watch.

Since I told them that I couldn't really understand Japanese, they (who I assume
to be the owner since he gave me his card afterwards) put me in front of a young
chef with the best English[^2] out of the staff and said "we have no English
menu but he is your menu, please ask him everything". He asked me if I was okay
with a bunch of small dishes, and since I didn't know what was the norm here,
nor how to read the Japanese menu, I nodded in agreement.

The restaurant is run by the charismatic Tamayose-san, a happy guy maybe in his
early fifties who I assume owns the restaurant. Between many jokes and back and
forth broken English and (my) broken Japanese, he handed me his card and told me
that he was born in Okinawa and that his father is from China, explaining the
style of cuisine on offer.

Throughout the night, Tamayose-san, the young chef (whose name I didn't learn),
and the other chefs next to him did their best to make sure that I was having
fun. Their friendliness made it easy for me to practise my Japanese, compared to
the high pressure environment that you might be in when you're ordering or
asking for help and trying not to waste people's time. We talked a lot and I
asked how they had learned their English; Tamayose-san said that he hated class
but he picked it up from watching movies and from interacting with customers out
of necessity, while my chef friend (who I suspected to be Tamayose-san's son)
had taken some classes during school. I told them both that their English was
"jyouzu" (good) and they asked me if I was half Japanese since apparently my
accent wasn't too bad 😁.

The young chef told me that he also worked at a yakitori restaurant and that
he'd be there tomorrow, so I could go there at night if I liked that sort of
food. The other chefs laughed when he mentioned it, as if that was maybe
something that he liked telling customers.

After I was already feeling stuffed and wondering when I would stop getting fed,
Tamayose-san asked me if I could still eat and I jokingly said "yabai"[^3]. He
warned me that since it's so versatile it can be interpreted as "I want more"
rather than "I'm full". I felt silly not saying that I was full earlier, but it
was in part due to having no idea what the pricing structure was like. Was I
supposed to just pay a set amount or was I paying per dish? Were there a set
amount of dishes? This was in part due to the way that my chef friend had
phrased it when asking me if I was okay with a lot of small dishes, which I had
misinterpreted as some sort of set menu. Definitely stuff that I should've asked
but was made harder by the language barrier.

![Ryukyu Chinese Tama, Shibuya: at first I thought that this delicious looking
crepe was for me but it wasn't the case. Had I known that I was paying per dish
I probably would've ordered it.](./IMG_20190827_195316.jpg)
![Salmon with pickled rice noodles. Chinese dried tofu with edamame. Chashu with
beansprouts. All pretty good.](./IMG_20190827_195930.jpg)
![A salad with celery, avocado, tomato, cucumber and salmon. I liked it.](./IMG_20190827_200454.jpg)
![Homemade Chinese pork sausage (lap cheong). Good but quite filling.](./IMG_20190827_201635.jpg)
![Mabo dofu with rice. There was an overpowering pepper taste which made it hard
to taste anything else.](./IMG_20190827_202338.jpg)
![Shumai. A bit heavy and lacked flavour other than "pork".](./IMG_20190827_202652.jpg)
![After a bit of a break, I ordered this fruit bowl for dessert, then regretted
it after taking a bite and realising that I was still stuffed. All of the fruit
was good, but it's not as good when you're full.](./IMG_20190827_211451.jpg)
![View from the outside. Not pictured are hundreds of wine bottles lining the
walls.](./IMG_20190827_213159.jpg)

I say with certainty that I had an incredible time enjoying the atmosphere and
chat, but it was still quite a shock when the 6000 JPY bill came. The food was
generally good, but I didn't find any of it to be _amazing_. I took a picture of
the receipt and tried to work it out at home with Elbert, but it was just a
handwritten order slip without any individual prices. I also looked up reviews
of the place and it turns out that everyone gets the taco rice which is served
with Ozaki wagyu beef and represents great value for money. There's always next
time I guess.

It then took me about four hours to write my Sushi No Midori blog. I'm not sure
how to address the problem of blogging taking so much time, but it's something
that I need to figure out if I want to keep up this daily cadence.

##### Waking hours

1455–2900

[^1]:

  excuse me for the poor writing that follows here, it felt like a bit of a
  blur, my notes were disjointed, and it's hard to convey the entire experience
  coherently without spending excessive amounts of time on editing

[^2]: it was easy to understand his accent but he lacked vocabulary
[^3]:

  an incredibly flexible slang word which can mean a whole lot of things,
  including "oh shit" or "that's crazy"
