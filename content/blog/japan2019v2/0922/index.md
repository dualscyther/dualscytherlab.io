---
title: Some things that I miss while living in Tokyo
date: "2019-09-22"
---

(apart from friends and family):

- affordable potato chips, especially the deli style ones that Kettle and Red
  Rock Deli are popular for.
- affordable fruit and vegetables.
- good bubble tea that doesn't cost a fortune, although to be fair I haven't
  looked too hard.
- surprisingly, not coffee since you can find some great places if you look for
  them. Although often it will hurt your wallet. I know that's sort of
  inconsistent since you can get great fruit in Tokyo as long as you're willing
  to pay, but I guess I subconsciously classify coffee as a luxury which I'm
  willing to pay for whereas I take fruit for granted.

## More Lattest and more pasta

Elbert tagged along with me to do a whole lot of nothing around Omotesando. We
got to Harajuku at 1540 and found some food. It was incredibly busy walking down
the Omotesando street (the busiest that I'd seen it anyway) presumably because
it was a Sunday.

![A Brazilian sandwich shop, Harajuku: my mortadella sandwich was delicious.
This was also the first time that I'd had a soft drink in many
months.](./IMG_20190922_161300.jpg)
![Elbert explained how "omatase itashimashita" (the honorific form of "sorry
to have kept you waiting") is just a conjugation of "matsu" (to wait).
Horrifying but good to finally know what they're saying at the
restaurant.](./IMG_20190922_163304.jpg)
![Lattest, Omotesando: Mizuki made my coffee again. It wasn't amazing this
time but still good. They were selling sandwiches today but I'm not sure if
they always have, otherwise I'm stupid because I've now had to find food two
times elsewhere before going to Lattest because I thought that they didn't
serve proper food.](./IMG_20190922_170836.jpg)

Elbert left to meet some friends in Shinjuku so I left not long after at 1820.
There was a line outside the newly reopened Apple store, the first time that
I've seen one since I've been here. I assume it's also due to the launch of the
new iPhone.

![Imabari Towel, Minamiaoyama: I think I spent half an hour buying these due
to analysis paralysis and slowly communication to the attendant in a mix of
Japanese and English. I really enjoy trying to speak in Japanese and it feels
like the staff appreciate it too and will make an extra effort to speak to
you in English, including using their phone to translate. They gave me extra
plastic gift bags and were putting an extra plastic cover on top of this
cardboard bag when I refused, and pulling out my translation app, and said
something like "save environent" (kankyoo o mamoru). They all acted impressed
and one said "subarashi" but I just laughed and waved my phone to indicate
that I actually had no idea what I was saying.](./IMG_20190922_193805.jpg)

Since I had to change at Shibuya to get home, I grabbed dinner there too.

![Above Shibuya metro station, Shibuya: spaghetti with muscles, prawn and
basil. The prawns tasted like frozen prawns rather than having the sweet
fresh taste, and the pasta didn't seem handmade although maybe it just wasn't
fresh. It was not bad otherwise.](./IMG_20190922_203718.jpg)

I got home just before 2200, watched the F1 Singapore Grand Prix and stayed up
late blogging.

Ate a pack of meiji biscuits. Watched Singapore Grand Prix. Ate 2 konnyaku
jellies after shower. Ate another 2 before bed. Very hungry, I know I didn't eat
enough today.

![Home, Ootsuka: I hope these are good.](./IMG_20190923_000928.jpg)
![Three packs of two biscuits each.](./IMG_20190923_001015.jpg)
![I ate one pack. Nice and crispy biscuits with the perfect strength of
matcha flavour.](./IMG_20190923_001134.jpg)
![I had four more of these konnyaku jellies since I was feeling hungry. I
don't think I ate enough today.](./IMG_20190923_042444.jpg)

##### Waking hours

1420–2915
