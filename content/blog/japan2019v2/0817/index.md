---
title: I love conbini pasta
date: "2019-08-17"
---

I left the house at around 1700 to go to Lattest again, stopping at the local
Lawson to get some soy sauce karaage (I forgot to take a picture). I had decided
to get on top of my blog posts, and chilling out at a cafe on my laptop was
something that I'd wanted to do on this holiday anyway. I spent the time reading
about Japanese grammar and realising that I should've done it much earlier in my
learning progression.

![Tokyu Plaza, Omotesando: cool entrance that I saw on the way to Lattest from
Harajuku station.](./IMG_20190817_173439.jpg)
![Lattest, Omotesando: same medium roast beans, double espresso. Good but not as
good as two days ago.](./IMG_20190817_175143.jpg)

Unfortunately, Lattest closed at 1900 but I was feeling productive, so I walked
down towards Aoyama-dori (Omotesando's main street) trying to find another cafe
that wasn't going to close too early. Not many actually had their opening hours
on display so I kept walking until I found one that did: Starbucks.

I read more about Japanese grammar and did some more blogging at Starbucks, but
I have to be truthful and say that I was also half listening to the pair of
expats having a conversation in English right next to me. They were catching up,
talking about their interests, how they ended up in Japan, their ambitions here,
their overall impressions of the country, and how things were going back at
home; I found it completely fascinating. Maybe that's why I find Terrace House
interesting too–it provides us with a window into other people's lives.

![Starbucks, Omotesando: matcha cake and cold brew (grande size). I liked the
cake but the coffee was... okay.](./IMG_20190817_191323.jpg)

Starbucks closed at 2200 so I went home and gobbled down a conbini pasta that I
picked up on the way home. I must've been hungry from not eating much all day
because I also ended up eating the massive pudding that was sitting in the
fridge, just before bedtime as the sun was rising.

![Home, Ootsuka: spaghetti napolitan (that's what they call it here) from
FamilyMart. It's great that they heat it up for you too.](./IMG_20190817_224036.jpg)
![The taste and texture is surprisingly good for the price.](./IMG_20190817_224135.jpg)
![I took a picture of this pudding next to the phone to show how large it is but
I realise that it actually makes it look sort of small. Well anyway, it's a big
pudding.](./IMG_20190818_043747.jpg)

##### Waking hours

1500–2920
