---
title: Asakusa
date: "2019-08-30"
---

It's lightly raining outside and it's not even that early (1450), should I still
go to Asakusa like I planned? Alright, let's go.

![Asakusa JR Station: this escalator has multiple flat and sloped sections. I
wonder why that is.](./IMG_20190830_151259.jpg)

Okay it's 1530, I actually have a decent amount of time before it gets dark.
Let's check out this temple.

![Kaminarimon Gate, Asakusa: wow it's busy. So many people wanting to take a
photo directly under the lantern. A lot of yukatas around too. I hope it's not
too slippery in those traditional shoes.](./IMG_20190830_152654.jpg)
![Why did they think that it was cool to make this sign sloped?](./IMG_20190830_152923.jpg)
![Even busier through the gate. Look at that guy in front of me taking a
picture of what I'm taking a picture of. Hard to get a photo that emphasises
the depth/distance to the temple.](./IMG_20190830_153015.jpg)

The stuff on sale here is pretty standard Japanese market/touristy stuff, so I
can't be bothered taking pictures, otherwise I'd have to take pictures of
everything! Sound reasoning I know. Hmm.. that announcement just said no walking
and eating/drinking (in English and in Mandarin). There's signs around the place
too. Why are so many people doing just that??

I do need breakfast though, and I don't really want all these Japanese sweets on
display. Wait, what's over there?

![Not bad. Hot deep fried pork pastry. There wasn't much filling and I don't
really feel like eating batter, so I won't go back to get a different flavour.](./IMG_20190830_154858.jpg)
![Another gate. Look at that five-storied pagoda on the left.](./IMG_20190830_155455.jpg)
![Looks about the same, but bigger.](./IMG_20190830_155543.jpg)
![I guess those ropes stop the lanterns from swinging everywhere.](./IMG_20190830_155618.jpg)
![Not sure what I expected to see.](./IMG_20190830_155633.jpg)
![So many o-mikuji (the fortunes on paper that you pull out). Also, is that
Tokyo Skytree?](./IMG_20190830_155926.jpg)
![I didn't realise that it was a trust system. Put in your money, pull out a
number, then go to the box with your number to take out a fortune.](./IMG_20190830_155939.jpg)
![Quick, cover yourself with incense smoke while it's there. Also, this guy's
got the right idea.](./IMG_20190830_160127.jpg)
![Did she really just put her mouth on the dipper? Whatever, my turn.](./IMG_20190830_160258.jpg)
![Senso-ji (the temple itself), Asakusa: this is a rather long line. When in
Rome I guess.](./IMG_20190830_160440.jpg)

![Feels a lot more relaxed here looking out the side steps rather than being out
the front.](./IMG_20190830_161007.jpg)
![Should I get a charm? Nah, I'm sure I'll be going to another temple at some
point.](./IMG_20190830_161048.jpg)
![A blog post once told me that it's easy for ceilings to go unnoticed. This one
is simple but nice.](./IMG_20190830_161202.jpg)
![Asakusa Shrine, right next to Senso-ji. This shrine is Shinto whereas Senso-ji
is Buddhist.](./IMG_20190830_161401.jpg)
![I can finally use this after waiting three minutes for these clowns to take
photos of each other.](./IMG_20190830_161633.jpg)
![Are those stones glued? How does that work?](./IMG_20190830_161659.jpg)
![A less impressive gate, but taking pictures through gates is fun.](./IMG_20190830_161803.jpg)
![A little green patch off to the side. That's Senso-ji on the right.](./IMG_20190830_161840.jpg)
![I need to take a picture back the other way. For context... or something.](./IMG_20190830_162052.jpg)
![I don't know what this is but it looks cool.](./IMG_20190830_162158.jpg)
![I like this perspective.](./IMG_20190830_162316.jpg)
![Little footbridge to some other building.](./IMG_20190830_162539.jpg)
![Looking out over the bridge. Everything is so well groomed.](./IMG_20190830_162528.jpg)
![Huh, are those real pigeons? Nope, stone.](./IMG_20190830_162916.jpg)
![Pretty photo spot on an ugly building.](./IMG_20190830_163014.jpg)
![Kagetsudo, Asakusa: is this uhh, how I "use" this?](./IMG_20190830_163113.jpg)
![She's making my food. I hope it's good.](./IMG_20190830_163155.jpg)
![Matcha soft serve melon pan. Bread was okay, ice cream was good.](./IMG_20190830_163710.jpg)
![Huh, it's sort of dead here. Wait, is that a cafe behind me?](./IMG_20190830_164613.jpg)

![Fuglen Coffee, Asakusa: I was just going to have an espresso but you've
convinced me.](./IMG_20190830_165014.jpg)
![This doesn't blow my mind, but it's great.](./IMG_20190830_165122.jpg)
![And so were these. I still liked the espresso the most. The aeropress is on
the left and the drip is on the right.](./IMG_20190830_165509.jpg)

Uhh what, he's asking me what cafe I work at in Sydney? Crap, I don't know how
to reply in Japanese. "No... work." I can sort of ask what beans were used for
the espresso though.

!["Espresso no beans wa, dore desu ka?" Loving this picture on the bag.](./IMG_20190830_172000.jpg)
![That is a lot of coffee merch. I'm pretty buzzed now, probably doesn't help
that I've barely eaten.](./IMG_20190830_172810.jpg)

Maraguto Nippon is nearby, let's go there to window shop some Japanese goods. No
photos allowed, noooo.

![I know that this was just left here as litter, but it would be cool to have
clean ones to take while you're on the toilet. Sort of like a map of the ski
runs while you're in a chairlift.](./IMG_20190830_174932.jpg)

These wood/leather wallets are amazing but my current \$30 wallet is fine and
these are 20000+ JPY. Maybe in a few years. Funny how you can't take photos
except in designated "photo spots" that have less interesting stuff.

![The sprinkling is honestly barely noticeable but everyone has their umbrellas
out.](./IMG_20190830_180650.jpg)
![A little bit cool, a little bit obnoxious.](./IMG_20190830_180709.jpg)
![Why are there so many gates everywhere!?](./IMG_20190830_181027.jpg)
![Seems like they love the Turkish ice cream performance in all parts of the
world (this isn't sarcasm, there was a small group there before this picture).](./IMG_20190830_183021.jpg)

![I can't find the sushi restaurant that I wanted to go to, but there's the
Skytree again. Let's walk back to Senso-ji, I've heard it's lit at night.](./IMG_20190830_184249.jpg)

![Literally.](./IMG_20190830_184917.jpg)
![Be careful, people might think that you're trying to take an upskirt picture.](./IMG_20190830_184951.jpg)
![Less busy now.](./IMG_20190830_185116.jpg)

![Let's just go back to the station and get food with Elbert somewhere.](./IMG_20190830_185411.jpg)

There's no pocket WiFi reception on the subway but luckily the train itself has
WiFi. Interesting how Google location thinks I'm in Shibuya, I guess it doesn't
understand constantly moving access points.

![Bicqlo, Shinjuku: That's right, it's Bic Camera and Uniqlo.](./IMG_20190830_194938.jpg)
![Golden Gai, Shinjuku: so many shops here but turns out most of them don't
really do proper food. We walked all the way here just to turn around and go
back. Cool place though with all these 1.5 person wide alleyways, but it's
actually all private property and they have a no photos rule.](./IMG_20190830_201837.jpg)

Hmm I think we're nearing Kabukicho, every second building is a love hotel.

![A main street, Shinjuku: this is probably the colourful Tokyo that everyone
thinks of.](./IMG_20190830_204219.jpg)
![Omoide Yokocho, Shinjuku: "Memory Lane". One street over is "Piss Alley",
where we're heading to eat skewers.](./IMG_20190830_204654.jpg)
![Edamame for our cover charge. I only recently learnt that these are just soy
beans. We also needed to order a drink each.](./IMG_20190830_210328.jpg)
![Are the staff speaking Mandarin? Oh cool, I guess they're Chinese. Which is
lucky for those Chinese travellers that just came in. Also wow, these asparagus
bacon skewers are delicious.](./IMG_20190830_210732.jpg)
![Pork belly with miso. Not too fatty, not too lean.](./IMG_20190830_211024.jpg)
![Chicken set. Spring onion, heart, liver, skin, tail. You can have the skin
Elbert, I'll have the innards.](./IMG_20190830_211824.jpg)

![This doesn't even make sense, where are the pearls in the middle? Is there
meant to be any tea in that?](./IMG_20190830_204920.jpg)
![A classic black pearl milk tea. Too sugary because I forgot to customise it
although I'm not sure if you even can. Otherwise not bad, but it's no CoCo.](./IMG_20190830_215809.jpg)

![Lawson, Ootsuka: I don't want to eat Polish babies.](./IMG_20190830_222933.jpg)
![Home, Ootsuka: that bath was great. Let's practise some verb conjugations.](./IMG_20190831_021456.jpg)
![And eat some of those absolutely delicious tomatoes that I washed last night.](./IMG_20190831_022748.jpg)

Time to sleep before the sun comes up and the trains start making noise.

_What did you think of this format/style/voice? I'm not a big fan so I'm not
sure if I'll do it again, but it was probably a little easier to write._

##### Waking hours

1330–2820
