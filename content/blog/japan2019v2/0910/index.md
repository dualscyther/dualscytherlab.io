---
title: Roadtripping day 2
date: "2019-09-10"
---

Our food was waiting for us when we got to breakfast a bit past 0800.

![Lalaca, Hakone: kaiseki breakfast.](./IMG_20190910_081711.jpg)
![ ](./IMG_20190910_082145.jpg) ![ ](./IMG_20190910_082156.jpg)
![ ](./IMG_20190910_082159.jpg)
![I chose "five grain rice".](./IMG_20190910_082205.jpg)
![They chose congee or something.](./IMG_20190910_082226.jpg)
![ ](./IMG_20190910_085534.jpg)
![These were shared for some reason.](./IMG_20190910_085902.jpg)
![Nice view from the dining area.](./IMG_20190910_084451.jpg)
![I liked the look of these closed blinds in the lobby.](./IMG_20190910_090121.jpg)

We were really full after the meal and relaxed a bit before checking out
at 1000. One of the staff stood at the driveway entrance and bowed as we drove
out, which I felt was quite excessive.

We'd planned to explore Owakudani–a volcanic valley–but when we there the area
was closed due to volcanic activity, so we went to our next stop at Lake Ashi
instead to see Hakone Shrine. We parked nearby and since free parking doesn't
really exist in the city, we weren't sure whether we needed to pay, but I asked
a nearby cafe and a lady said that it was free.

![Hakone Shrine, Hakone: koi.](./IMG_20190910_103421.jpg)
![Looks like some dead wood from the front, and just strange from here.](./IMG_20190910_103530.jpg)
![A little shrine on the way up the stairs towards the shrine.](./IMG_20190910_103840.jpg)
![ ](./IMG_20190910_104137.jpg) ![ ](./IMG_20190910_104210.jpg)
![Another little shrine off to the side.](./IMG_20190910_104644.jpg)
![It was really sunny and hot so we didn't bother with the main shrine.](./IMG_20190910_104822.jpg)
![A line to take pictures of the Torii in the water. This took a while since
everyone was taking a lot of pictures. There were a lot of couples doing cute
poses.](./IMG_20190910_105413.jpg)
![ ](./IMG_20190910_110808.jpg)
![Look at those tall, straight trees.](./IMG_20190910_110823.jpg)
![ ](./IMG_20190910_112005.jpg)

Then we made our way to Mishima Skywalk, a 400m pedestrian bridge which is the
longest in Japan[^1].

![Hakone: a road gate.](./IMG_20190910_113542.jpg)
![Apparently in Japanese mythology there's a giant catfish that causes
earthquakes.](./IMG_20190910_113831.jpg)
![ ](./IMG_20190910_114913.jpg)

What I found interesting was that the bridge was a privately funded project
built entirely for tourism, and not for any other practical value. It's built in
a remote location and after you walk across, you just walk back. The reason for
the location was that on a good day you can see Mount Fuji and Suruga Bay, and
it was also hoped that it would help bring traffic to the town of Mishima which
was recently bypassed with a highway.

![Mishima Skywalk, Mishima: you get a voucher for 50 JPY off the soft serve
if you buy the 1000 JPY bridge walk ticket. I got lucky because I was
actually very tempted to buy the ice cream as soon as I saw it since it was
so hot, before we knew that we'd get ice cream
vouchers.](./IMG_20190910_120723.jpg)
![ ](./IMG_20190910_122015.jpg)
![Where's Mount Fuji?](./IMG_20190910_122453.jpg)
![ ](./IMG_20190910_122621.jpg) ![ ](./IMG_20190910_122830.jpg)
![View of the valley. All the forests that I've seen have had these super
tall and straight trees.](./IMG_20190910_123212.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190910_124424x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Cooling down.
  </figcaption>
</figure>

![You can pay to ride segways here. I guess they just added extra activities
for you to do to capitalise on tourists coming here from the
bridge.](./IMG_20190910_124504.jpg)
![Similarly, you can pay to do an obstacle course. These people were crazy
since it was so bloody hot. There was also ziplining along the length of the
bridge but I didn't take a photo.](./IMG_20190910_130408.jpg)
![Some wooden egg things. I don't know.](./IMG_20190910_125639.jpg)
![A diorama at the souvenir shop.](./IMG_20190910_131129.jpg)
![You can write wishes on these then drop them off the bridge. They're just
wood containing a seed, so I guess your wish is supposed to sprout on the
valley floor or something.](./IMG_20190910_133339.jpg)
![ ](./IMG_20190910_134227.jpg)

![An airconditioned "Sky Garden" area with flowers everywhere.](./IMG_20190910_135615.jpg)
![Bob sitting outside the luxury women's bathroom which was even advertised
on the pamphlet included with our tickets. To bad we couldn't see inside, but
apparently it even has a garden.](./IMG_20190910_140721.jpg)
![Interesting gift.](./IMG_20190910_140919.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190910_141309x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Outside the gift shop.
  </figcaption>
</figure>

Then we headed towards our next ryokan by driving through the mountains. As we
drove through we saw a shop by the side of the road so we stopped for lunch (at
1500).

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190910_153042x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    We took a tolled road to get a nice drive (and shorter I think) through
    the mountains. This road also had a brief stretch of "melody road" where
    driving over it will play a tune. It's hard to hear over the sound of the
    wind but it's there.
  </figcaption>
</figure>

![There was an amusing moment where we saw an old Japanese guy on a motorbike
stop to pet and talk to this goat, then drive
away.](./IMG_20190910_143708.jpg)
![Tororo soba. The thing on the bottom left is yam and it was definitely a
bit strange. We were told to mix it with the soba but it sort of just sticks
to itself. Reminds me of the texture of natto.](./IMG_20190910_145326.jpg)
![There was a lookout point next to the shop. We pretty much got to see Owakudani!](./IMG_20190910_154047.jpg)

As we continued driving, we came across a lookout point and also saw that the
clouds around Mount Fuji had cleared, so we stopped for probably half an hour
just to appreciate the view as well as take photos.

![One out of a million photos that we took.](./IMG_20190910_164452_1.jpg)

![When it's clear you can really see it from anywhere.](./IMG_20190910_171507.jpg)

We also drove past Fuji-Q Highland amusement park which I'll be sure to visit at
some point this trip. We got to the ryokan at 1800 but this time we didn't have
dinner included (although we did end up adding it on for the next night) so we
went out to eat after checking in and enjoying the view from our room.

![Rakuyu Hotel, Fujikawaguchiko, the view from our room.](./IMG_20190910_180939.jpg)
![Our room.](./IMG_20190910_181307.jpg)
![Bath towels, scent treated wash towels, yukatas, plastic bags for our dirty
clothes after bathing, and cute baskets to take with us to the
onsen.](./IMG_20190910_181425.jpg)
![A hotel snack left on our table.](./IMG_20190910_183200.jpg)

![A ramen restaurant, Fujikawaguchiko: renge ramen.](./IMG_20190910_203129.jpg)
![Home-made gyoza.](./IMG_20190910_203134.jpg)
![Aeon MaxValu supermarket, Fujikawaguchiko: buying some snacks for our next
two days. Look at those decorations! This supermarket was the size of a big
Sydney supermarket in the suburbs, rather than the usual small ones that were
in Tokyo.](./IMG_20190910_211848.jpg)
![Hmm.. this pudding, or that pudding? They posed for this when they saw me
taking a picture.](./IMG_20190910_213010.jpg)

![Rakuyu Hotel, Fujikawaguchiko: Hanging up some coin laundry laundered
clothes. My unwashed t-shirts are on the left.](./IMG_20190910_233455.jpg)
![Enjoying some milk after bathing in the hotel onsen.](./IMG_20190910_233425.jpg)
![ ](./IMG_20190910_233759.jpg)

I took some time to finish a blog post because I didn't want to get too far
behind on posts. Bob and Christine slept half an hour earlier than me since it
was late.

##### Waking hours

0750–2530

[^1]:

  although I think that having to qualify it with "in Japan" and "pedestrian" is
  really just silly and they could do away with trying to call it the longest
  bridge.
