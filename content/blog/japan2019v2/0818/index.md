---
title: The Roastery by Nozy Coffee
date: "2019-08-18"
---

I'd read about The Roastery in reviews and Jack had recommended it, so I went
there at around 1700 to do some blogging on my laptop and try the coffee. I felt
pretty weird taking so many pictures, but everything was cool so why not. It
felt like I was back in Sydney except the coffee costs a lot more. Their main
menu and the machines in the middle imply that they do espresso based coffee the
most, but they also do cold brew, pourover, and French press (lumped together
under "hand brew" which costs 650+ JPY).

![Tokyu Plaza, Omotesando: here again on the walk to the cafe.](./IMG_20190818_170744.jpg)
![The Roastery by Nozy Coffee, Omotesando: I like the mug in the of the "o".](./IMG_20190818_172038.jpg)
![It might be nice to sit out here when it's not over 30C.](./IMG_20190818_172044.jpg)
![The primary menu hanging on the wall was almost entirely in English.](./IMG_20190818_172134.jpg)

I bought a coffee and pastry, then sat on my laptop for an hour or two.

![A great espresso, brewed by barista Takuya. When he saw that I was taking a
photo, he moved the information sign for me.](./IMG_20190818_172501.jpg)
![Definitely amongst Sydney's good espressos, which was a relief since it set me
back 630 JPY.](./IMG_20190818_172542.jpg)
![Some prepared cold brews. I think each bottle was brewed using different
beans.](./IMG_20190818_173032.jpg)
![Beans and paraphernalia for sale.](./IMG_20190818_173046.jpg)
![The place was pretty busy when I first got here.](./IMG_20190818_173101.jpg)
![New York ring (390 JPY): I was so hungry that I don't remember how it tasted.](./IMG_20190818_173236.jpg)
![So many beans to try and to buy.](./IMG_20190818_174223.jpg)
![Cupping every Wednesday! Maybe I'll go to that.](./IMG_20190818_174417.jpg)

Feeling peckish again and having seen lots of customers coming in and buying a
soft serve, I bought another coffee along with an ice cream.

![All of my friends in front of me left 😔.](./IMG_20190818_175427.jpg)
![This one only cost 480 JPY.](./IMG_20190818_182422.jpg)
![Served in a different cup for whatever reason. Having drank the previous cup,
I was a little disappointed; the flavours were less clear and popped out at me
less, although overall it was still decent. It had more to do with the beans
than the extraction (strange because I think I normally have trouble
distinguishing between the two.](./IMG_20190818_182449.jpg)
![Half milk, half latte soft serve (500 JPY). The lady (who had pretty good
English) made it extra large for me because she forgot to make it and I had to
ask about it. I definitely didn't mind, although it ended up making me cold
since I'd been sitting in aircon for a few hours.](./IMG_20190818_184505.jpg)
![Working on my 0813 blog post.](./IMG_20190818_194253.jpg)

I left at 2030 since I needed some actual food, so I headed home and ate near
Ootsuka station on the way. I learnt Japanese and did some blogging for the rest
of the night after I got home at around 2230.

![Tuna bowl teishoku (a meal set). The rice was bad and the tuna was eh.](./IMG_20190818_212147.jpg)
![Elbert surprised me with this when he ducked out to the conbini for his
dinner late at night 🥰.](./IMG_20190819_035826.jpg)

##### Waking hours

1330–3030
