---
title: Asakusa again
date: "2019-09-14"
---

Elbert and I met up with Bob and Christine in Asakusa, where they had rented
kimonos.

![Senso-ji, Asakusa: Christine asked some nice tourists if she could take a
picture with them.](./IMG_20190914_115402.jpg)
![Nearby: fried rice, gyoza, and gyoza okonomiyaki for lunch.](./IMG_20190914_122630.jpg)

We returned the hired kimonos and then spent the rest of the day enjoying
Asakusa.

![Fuglen Coffee, Asakusa: good coffee at a decent price (by Tokyo standards),
so why not come here again.](./IMG_20190914_131829.jpg)
![We wanted to split the tasting set (drip, aeropress and espresso) between
three of us but the maximum allowable was two people. I found it a bit silly
because the cafe wasn't that busy anyway. We bought a cappuccino so I guess
they got the result they were after. Elbert had a chai
latte.](./IMG_20190914_132320.jpg)
![Rox Dome, Asakusa: the ball is hard to hit even at 70km/h. We also tried 90
and 110.](./IMG_20190914_135831.jpg)
![They give a great visual aid for when the ball is coming out; the pitcher
on the screen winds up and then the ball comes out of his "hand" which lines
up exactly with where the hole in the wall is.](./IMG_20190914_135834.jpg)
![Don Quijote: Asakusa: Bob and Christine did some tax free gift shopping for
food and alcohol. I think this is meant to say "new collection". I also
bought a small tomato amazake and it tasted great. I saved the 300mL bottle
since it fits so well in my bag.](./IMG_20190914_145204.jpg)

We followed Bob and Christine along to their nearby hotel, where they repacked
their shopping into their suitcases in the lobby.

![Tooyama Tonkatsu, Asakusa: good pickles.](./IMG_20190914_163401.jpg)
![This was alright. The timing of your meals, daily schedule, and sleep
really affects your appetite. I could barely finish this and I took forever,
whereas Christine did it easily. But I never had trouble eating the kaiseki
meals in the last few days.](./IMG_20190914_163613.jpg)

After eating we did some more shopping at Asakusa, although we didn't buy
anything.

![Asakusa: we stumbled across this parade which mainly featured old ladies
doing some coordinated movements to music. Kawaii.](./IMG_20190914_180300.jpg)
![The view from the back.](./IMG_20190914_180619.jpg)

![Asakusa station, Asakusa: we walked back to the hotel, picked up the bags,
then said our goodbyes 😔. Bob and Christine were flying out that
night.](./IMG_20190914_191428.jpg)

![Asakusa-dori Ave, Asakusa: a quick detour to a nearby bridge to look out
over the Sumida River.](./IMG_20190914_192112.jpg)
![What is that turd looking thing? EDIT: I've been told that it's the Asahi
Building.](./IMG_20190914_192237.jpg)
![Shimbashi station, Shimbashi: this feels scarier than if there were no
barriers there at all.](./IMG_20190914_195639.jpg)

We got home at 2055. It felt like a long day and I was quite drained, probably
due to lack of good sleep too. Elbert was so dead that he slept before me,
at 2330.

![I'd stupidly bought grapes and tomatoes before going on the roadtrip, and
even though I left them in the fridge, they were starting to show their age.
The grape and tomato skin was no longer firm and supple. I also understand
now why these grapes were cheap, they're just balls of
skin!](./IMG_20190914_235121.jpg)

##### Waking hours

0900–2800
