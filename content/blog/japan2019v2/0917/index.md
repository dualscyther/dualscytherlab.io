---
title: Minor caffeine poisoning
date: "2019-09-17"
---

I left the house for Harajuku station at 1600, hoping to just sit in some cafes
and learn Japanese. The whole Harajuku and Omotesando area is nice to walk
around in.

I walked toward Lattest, hoping to find some food that I was interested in
eating along the way (since Lattest itself doesn't really do food except for
cookies), but by the time I got there, nothing had caught my eye yet so I just
made do with whatever was close.

The server at the cafe that I went to didn't know much English so I had some
trouble ordering. I asked for an espresso but she said that she could only do it
in a large cup, so I thought why not and got a latte instead. I tried to choose
from the beans on the menu but then she said that I couldn't choose, so I asked
what the choice of beans was for and she told me it was for the drip coffee.
Then I asked her about the beans and she pulled out a little notebook that had
the five beans on a scale of acidic to bitter. Keep in mind that this was all
said with a mixture of gestures and pointing, and a tiny bit of English and
Japanese. She clearly knew her coffee though and she also did know domain
specific English words like "drip", "espresso", "bitter", "sweet", and "clear".

![Lotus, Omotesando: strawberry tarte and Dominica drip coffee. In the
morning I'd been lamenting the lack of strawberry and cream sandwiches at the
conbini (there were plenty when I was here in winter) so I was excited when I
saw a cake with strawberries on it. Yummy but a bit pricey for 810 JPY. The
500 JPY coffee was good, great aroma and went well with the
tarte.](./IMG_20190917_173747.jpg)
![They play music from a record player.](./IMG_20190917_173755.jpg)

Afterwards I sat at Lattest for a bit, studying Japanese on my phone. Mizuki
also happened to be working there at the time. She spoke English to me when I
walked in so she might've remembered me, but I almost hope not since I was such
an idiot last time.

![Lattest, Omotesando: Kenya medium roast. Such a bargain in Tokyo for 350
JPY. This one was pulled perfectly.](./IMG_20190917_175518.jpg)

I walked outside a bit before 1900 (the closing time of Lattest) and realised I
was both hungry and incredibly caffeinated, to the point where I was feeling
jittery. It was funny realising that because I was shaming myself for not being
able to concentrate inside Lattest and attributing it to nerves, but it was just
caffeine all along. I was surprised how much it affected me because I probably
only had 2.5 espressos worth of caffeine, but I guess that's what happens if you
don't eat much and you lose your caffeine addiction/tolerance.

I stopped at a watch store along the way which I'd read had a large display of
Casio G-Shocks. I didn't realise that it was going to be an upscale one selling
almost exclusively multi-thousand dollar ones, except for the G-Shocks. It was
very spacious and mostly empty, making it really overstaffed. They actually had
a map at the entrance detailing which cases contained what brands. As I was
reading the map, one of the staff approached me so I asked if English was okay,
not really minding one way or another. When he told me to hold on a second, I
thought he was going to get someone else to come and talk to me, but instead, he
came back with a tablet and a live translator in an office on the other end of a
video call. Crazy!

I guess it's worth it when you're trying to sell these sorts of watches to rich
foreigners who happen to walk in. I felt bad because all I said was something
along the lines of "I'm just looking, I wanted to see the G-Shocks" so he just
directed me to the display and then ended the call. I wonder how much it costs
to use a live, human translation service like that. It didn't seem specific to
watches since when he initiated the call, he seemed to tell the translator that
he was at a watch store.

As I was eating dinner I felt pretty awful since I was so caffeinated. I
genuinely found it hard to eat properly and my coordination with my chopsticks
was off. When I held my bowl and my glass, I felt like I was going to drop them
at any second.

![Goku Burger, Omotesando: I enjoyed the food, although I wasn't expecting it
to come on a hot plate, so that was a downer due to my aforementioned
aversion to oil spitting everywhere.](./IMG_20190917_194431.jpg)

I got home, chilled out a bit, then went to the conbini to buy dinner at 2630 or
so.

![Home, Ootsuka: bacon and mushroom pasta. A combination of hunger and
absolute deliciousness meant that this didn't last for very
long.](./IMG_20190918_022622.jpg)
![Just when I thought that it couldn't get any better, I ate this royal milk
tea Ohayo pudding and it blew my mind. This didn't survive for very long
either. Now I need to try all the flavours.](./IMG_20190918_025113.jpg)
![Some milk before bed time. It still has the strange but nice cheesy taste
of Japanese milk, but it definitely didn't taste as creamy as what I've been
having.](./IMG_20190918_025948.jpg)

##### Waking hours

1430–2900
