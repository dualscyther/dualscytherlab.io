---
title: Getting ready to holiday in Hokkaido
date: "2019-10-01"
---

We called at least seven sushi restaurants but they all said the same thing:
they only take bookings for foreigners through a concierge. It was a little
disappointing.

![Home, Ootsuka: finished Elbert's peperonchino pasta before I left.
Delicious.](./IMG_20191001_133348.jpg)

I left at 1430 for Shibuya, where I picked up my 7-day JR Pass and made a
reservation for the 0638 shinkansen from Ueno to Hokkaido.

![Roar Brothers, Shibuya: inside a building called Shibuya109 which has shops
almost purely catering for women, meaning that everyone walking around was a
schoolgirl or woman in their 20-30's, so I felt a little out of
place.](./IMG_20191001_153056.jpg)
![Coffee brown sugar milk tea. The coffee comes from a freshly pulled
espresso shot. The pearls were warm and really sweet, but it was okay since
they didn't add much syrup. I still wish it was a little less sweet. Very
good though and it's my favourite so far.](./IMG_20191001_153523.jpg)

The biggest Loft and Tokyu Hands stores are in Shibuya so I didn't miss my
chance to have a look at their stationery sections and then get lost again
looking at bottles and coffee equipment. Afterwards I walked to Omotesando to
pick up my socks from Tabio, and I got by without a single word of English
although the cashier did say "small bag" so I can't say the whole encounter was
English-free.

![Toriyoshi Shoten, Harajuku: dinner time. Do they get the menu printed
cheaper because they advertise LIMEX? There was chicken sashimi on the menu
which I would've tried but I was irrationally apprehensive because I didn't
want to risk ruining my upcoming trip with food
poisoning.](./IMG_20191001_203624.jpg)
![Fried chicken with black pepper. Great.](./IMG_20191001_205211.jpg)
![Tori soba. Eh.](./IMG_20191001_211151.jpg)

Overall a pretty uneventful day but I've come to enjoy just walking around (and
who knew that shopping is incredibly addictive too once you find the right
stores).

![Home, Ootsuka: cafe latte milk pudding.](./IMG_20191002_022119.jpg)
![Writing this caption makes me want to go buy another one and eat it
again.](./IMG_20191002_022214.jpg)
![Custard.](./IMG_20191002_024348.jpg)
![While both desserts were amazing, I preferred the latte one with no crust
or sauce at the bottom. Just pure mouth coating goodness and seriously up
there in my favourite foods. ](./IMG_20191002_024436.jpg)

I didn't end up finishing a blog post and I haven't even booked the last two
nights (out of six) of my trip to Hokkaido yet, which I'm starting tomorrow. I
couldn't really find "obvious" places to go so we'll just have to see. I slept
late, not exactly ideal since I have to leave the house at 0500 in the morning.
I'm excited to eat lots of seafood and dairy!

![Since I'm only away for six nights I packed everything I needed in this
backpack. Should make things a bit more convenient. If it was wasn't for the
weight of my laptop (I estimate slightly less than 2kg including charger) I'd
feel pretty comfortable wearing it all day.](./IMG_20191002_025453.jpg)

##### Waking hours

1120–2655
