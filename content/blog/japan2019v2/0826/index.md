---
title: Umegaoka Sushi No Midori, Ginza
date: "2019-08-26"
---

I left the house by myself at around 1600 with the vague idea to go to some
cafes in the Shibuya/Harajuku/Omotesando area. I had actually set an alarm to
wake up earlier but I just turned them off when they rang 🤷.

![Mos Burger, Ootsuka: my breakfast. This was my first beef burger (only 370
JPY) in ages. It was small which was perfect for my unexpanded stomach. There
was lots of pickle and onion which I liked, and the bun was soft like a Maccas
bun.](./IMG_20190826_162854.jpg)

I changed my mind and went to the Ginza/Yurakucho area instead, trying to make
it to a small park before sundown. It's not really a touristy spot, I just
wanted to walk around a park, and took a million pictures in the process[^1],
although I didn't take a picture when I came across a lady practising her flute.

![Hibiya Park, near Yurakucho: standing at the corner, I stopped and looked at
how this mish-mash of tall buildings lines the moat surrounding the Tokyo
Imperial Palace area (Chiyoda City).](./IMG_20190826_172512.jpg)
![Hibiya Park, near Yarakucho although officially within Chiyoda City: opened in
1903 as the first Western-style park in Japan (I just ripped that off of a
sign).](./IMG_20190826_171945.jpg)
![Up the stairs to look at a pond.](./IMG_20190826_172129.jpg)
![Looking out at more pond and park.](./IMG_20190826_172135.jpg)
![Dozing off after a long day.](./IMG_20190826_172316.jpg)
![This wall used to be here for a reason but I forget what the sign told me.](./IMG_20190826_172642.jpg)
![Crazy how you're in the middle of a serene park but you just look a bit
further and you see stuff like this.](./IMG_20190826_172826.jpg)
![I noticed that there were places to sit everywhere.](./IMG_20190826_172845.jpg)
![Here too, although they look horribly uncomfortable.](./IMG_20190826_173003.jpg)
![Our plaque is weathering away so here's another one.](./IMG_20190826_173108.jpg)
![If you complain about paper money, imagine shattering this.](./IMG_20190826_173246.jpg)
![A small outdoor concert area. Benches are behind the fence.](./IMG_20190826_173755.jpg)
![That building is huge even compared to other buildings.](./IMG_20190826_173923.jpg)
![No flowers or red leaves for me.](./IMG_20190826_174239.jpg)
![I don't see any azaleas 😔.](./IMG_20190826_174447.jpg)
![Sit here if you want to hear cicadas (they were super loud around the entire
park).](./IMG_20190826_174532.jpg)
![Or probably get bitten by mosquitoes.](./IMG_20190826_174619.jpg)
![A restaurant in the middle of the park. There were a few of these although
most were on the edges.](./IMG_20190826_174754.jpg)
![Another pond.](./IMG_20190826_174844.jpg)
![A little playground to work out. A few people were here training. I did some
pull-ups for fun and on the first rep I banged my chin on the way down.](./IMG_20190826_175111.jpg)
![The other side of the same pond. There's a fountain in this one 😮.](./IMG_20190826_175951.jpg)
![This was a really strange scene. There were a whole bunch of people just
standing around, all looking down at their phones. They must've been waiting for
something but it was very surreal, almost like I was looking at a staged photo.](./IMG_20190826_180122.jpg)
![A big lawn, Western-style I suppose.](./IMG_20190826_180546.jpg)

I was glad that I'd gotten out of the house early enough to actually experience
some daylight. The sun was setting as I walked to Ginza to have some coffee at
Café de l'Ambre[^2]. Some of the junior staff spoke English and they had an
English menu. I was asked whether I liked sour or bitter coffee, and whether I
preferred my cup to be small or diluted with water for a larger cup.

![On the walk to Ginza. I liked the glowing lettering here.](./IMG_20190826_181349.jpg)
![Café de l'Ambre, Ginza: making my traditional Japanese pourover using a
reusable bag filter. Like most places in Japan (especially more traditional
ones), the younger staff does the cleaning and preparation like passing ground
beans and passing the hot water. The more senior one gets the glory.](./IMG_20190826_183550.jpg)
![What do you expect, it tasted like a strong pourover. It had very clear fruity
notes.](./IMG_20190826_183704.jpg)
![Check out all those different beans, and the analogue scales.](./IMG_20190826_183926.jpg)
![These kettles looked old.](./IMG_20190826_183941.jpg)

After some good coffee, I walked a few blocks to Umegaoka Sushi No Midori, a
restaurant which apparently has good sushi at non astronomical prices. I'd read
that there were often long lines, but I still wasn't expecting a two hour wait.
Normally this would be a dealbreaker, but I had nothing better to do so I queued
up. Many groups in the queue were actually Chinese, since locals probably know
of other great places to eat which don't happen to be popular on Tripadvisor.

![Umegaoka Sushi No Midori, Ginza: automatic ticket machine (not to be confused
with automated teller machines) out the front. You had to go through a few
screens to get your ticket, but luckily there were English subtitles and an
additional set of printed English/Chinese instructions above the screen.](./IMG_20190826_191659.jpg)
![Water for thirsty queuers. I also saw people go in to use the bathroom. Seeing
all of this was when I realised that I should've got a ticket before I did
anything else today.](./IMG_20190826_191800.jpg)

A Chinese-American couple who arrived after me couldn't figure out how to use
the machine, so I made the mistake of helping them and having to pay for it with
small talk. I learnt that the guy–whose name was Sean–was from Virginia and that
after going to China for business in 2010, he ended up staying for nine years
after he met his partner.

Even though I got a ticket, I didn't go walking around Ginza since shops were
beginning to close and I was hopeful that being a one person party, I might be
called earlier. Unfortunately, that didn't happen so I eventually went in at
2110 after two hours of waiting. My newly met friends came in after me and
coincidentally sat next to me, so we chatted here and there throughout the meal.
We were mainly eating which meant that we could comment on the food to each
other without having to maintain an actual conversation the entire time.

![Egg custard and some salad-like appetiser. The custard had a little prawn in
it.](./IMG_20190826_211335.jpg)
![These chefs are machines, churning out piece after piece.](./IMG_20190826_211725.jpg)
![The first plate of sushi. I thought that this was it, since it also had the
egg on it.](./IMG_20190826_211931.jpg)
![Second plate.](./IMG_20190826_212430.jpg)
![A top up of salmon roe and uni.](./IMG_20190826_212435.jpg)
![I asked the chef if they had amaebi (sweet prawn) but he must've misheard me
because I ended up with another of the same prawn instead. I ordered this before
I realised that my stomach wouldn't be able to handle the sheer amount of food.](./IMG_20190826_212528.jpg)
![The chef gave a huge piece of egg to the couple next to me.](./IMG_20190826_212838.jpg)
![Miso soup.](./IMG_20190826_212842.jpg)
![Two well sucked prawn heads.](./IMG_20190826_214001.jpg)
![Sean gave me one of their rolls since he over ordered a little. The taste was
nothing to write home about, especially since I was so full too.](./IMG_20190826_215442.jpg)

The huge neta (the fish/other on top of the rice) pieces made things difficult
to eat and detracted from the sushi. You may as well be eating sashimi. In
hindsight, I should've bitten some off to eat alone, but instead I tried to have
everything in one bite. There weren't many great pieces, but there were quite a
few good or "eh" pieces. I didn't take piece by piece photos, so here are some
other notes:

- the prawn and eel pieces were solid.
- I was a fan of one of the eel pieces, which was slightly salty.
- one of the fish pieces was strangely crunching and didn't have much flavour.
  It was like eating cartilage and I wasn't a big fan.
- the uni wasn't amazing but it was solid too, it had a hint of that familiar,
  slightly not fresh taste.
- The salmon roe wasn't fishy but it also barely had any taste for some reason.
- The lean tuna was soft but also didn't have much taste. Maybe it requires
  ageing but I can't say that I know too much about actually making sushi, I
  just eat it.
- The egg was huge and I couldn't eat it in one bite. It didn't taste like
  anything special other than being sweet.
- I asked the chef for the name of the prawn and I heard "haka" ebi, but I can't
  find anything about it on the internet so it might be "aka" (red) ebi.

It was hard to pick a favourite, maybe the eel or the prawn, but even then they
weren't _amazing_. I felt bad for the couple next to me since the restaurant
messed up their order and they had to refuse two dishes: the waitress misordered
one set of rolls, and separately, the chef read the docket wrong and made the
wrong thing. I also found it amusing when Sean got super excited watching the
chef make his avocado and tuna roll, of all things. Don't get me wrong, I like
it too, but what about everything else!

The sushi ended up costing 2900 JPY for the deluxe set and an additional 232 JPY
for the extra prawn. Definitely cheap and probably worth it on paper, but I
can't say that I'd recommend going there unless you've never eaten good sushi
before and you're on a tight budget.

Dinner took about an hour, then I went home and stayed up blogging.

##### Waking hours

1500–2920

[^1]:

  this has to be the most pictures that I've ever taken in a park, because why
  would you really do that normally?

[^2]:

  I'll only bother to write "café" when it's a name, otherwise I'm sticking to
  "cafe"
