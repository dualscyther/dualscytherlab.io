---
title: Shin-Yokohama Raumen Museum and Chinatown
date: "2019-09-06"
---

I left the house at 1100 and it took me about eighty minutes to get to
Shin-Yokohama, where I met Bob and Christine to visit the Shin-Yokohama Raumen
Museum. There was a 310 JPY entry fee which I thought was rather cheap, but I
realised why when we went in and I saw that there really wasn't much museum at
all. Rather than being a museum with permanent exhibits and cases displaying
random things, most of the building is filled with carefully selected ramen
restaurants which showcase different types of ramen from around Japan.

Firstly we went downstairs to the ramen restaurants to eat lunch.

![Raumen Museum, Shin-Yokohama: recreation of an old Japanese street
setting.](./IMG_20190906_122613.jpg)
![We read this pamphlet to decide where to eat since the ramen ordering machines
had no English and no pictures.](./IMG_20190906_122850.jpg)
![So strange that this feels like an outdoor bar but it's actually indoors.](./IMG_20190906_123146.jpg)

I wanted to try a few different ramen types but unfortunately it's not really a
food court and you still have to eat inside the shop, which meant that we
couldn't get three different ramens then share them, so we just ate together at
the one shop. We couldn't just go inside and order one bowl between the three of
us either since they had signs explicitly telling us that you have to order at
least one bowl each at a restaurant.

![Quite a yummy shoyu (soy sauce) ramen. Bob also got this, while Christine
had a shio (salt) ramen.](./IMG_20190906_124939.jpg)
![A narrow alley around the perimeter of the level.](./IMG_20190906_131011.jpg)
![Bowls from different ramen shops which I assume have previously had
secondary stores here.](./IMG_20190906_131541.jpg)
![There's room for a few more.](./IMG_20190906_141129.jpg)

![Upstairs, which pretty much only has this small exhibit explaining the
history of ramen, how it evolved from Chinese cuisine, and why it tastes
good. There was heaps of text on the wall and I feel like it was probably a
bit too detailed, so I didn't feel particularly rewarded for reading all of
it.](./IMG_20190906_134538.jpg)
![I'll probably try all of these by accident at some point.](./IMG_20190906_134338.jpg)
![Hmm now I want wonton soup.](./IMG_20190906_134004.jpg)
![There was a "3 minute tasting" where we gathered around a counter and
a very eccentric host taught us the role of oil in ramen. At the end we got to
taste the broth with and without oil. The right cup has an oil layer on top.
Honestly it tasted similar to me.](./IMG_20190906_140214.jpg)

We went back downstairs after we reread the pamphlet and saw that you can get
mini bowls instead of full bowls at the shops. Unfortunately they turned out to
still be half size servings, and we were a bit too full at that point.

![The ceiling looks less silly from down below.](./IMG_20190906_140743.jpg)

Bob and Christine had planned to go to the Kirin Beer Village/factory in the
afternoon but I'd read that you need a (free) booking in advance, so we didn't
really want to take the forty minute commute to possibly get turned away at the
door. Instead, I tagged along with them to check in to their hotel, enjoy the
aircon, and have a bit of a rest, since they were sleep deprived from their
flight and not used to the heat yet. I pretty much had the exact same issue the
first few days that I got here, and the sun was out for the first time in ages
today so it was particularly hot as well as bright.

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190906_122157x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Right outside the museum.
  </figcaption>
</figure>

![Shin-Yokohama JR Station: I took a picture because it was just so wide,
tall, and blocky. Really not designed to look pretty.](./IMG_20190906_142215.jpg)
![A walk-in noodle shop literally on the platform.](./IMG_20190906_142709.jpg)
![As long as you're not drenched in sweat, I also find it easy to fall asleep on
the train in hot and humid weather.](./IMG_20190906_144001.jpg)
![Yokohama Station: that's one big cylinder.](./IMG_20190906_145609.jpg)
![Nissan Gallery Global Headquarters, Yokohama: this just happened to be on
the walk to the hotel. Too bad I'm not really a car person but cool
anyway.](./IMG_20190906_145823.jpg)
![So many old, new, and experimental cars to see.](./IMG_20190906_145753.jpg)
![Seems to be a museum/fun park for kids, not a museum about kids.](./IMG_20190906_150344.jpg)
![Getting some quick zzzs in while I learnt Japanese on my phone. The camera
made it look brighter than it was. I was also impressed by the blinds which
completely blocked the sunlight coming in.](./IMG_20190906_162633.jpg)

After a half hour nap we made our way to a cemetery for Bob to pay his respects
to his great grandparents. We walked past a rental bike station but it was a big
hassle trying to sign up online so we decided to just catch the train. Bob
actually went to the effort and signed up but by then we were already halfway to
the train station. Luckily, there was another bike station right there so we all
signed up.

![The entire area to the left of the frame (towards the bay) and most of this
picture exclusively had tall buildings.](./IMG_20190906_172228.jpg)

The bikes were really useful because the cost of renting for half an hour was
similar to the train fare to get to Chinatown, plus from Chinatown it was a half
hour walk uphill to the cemetery. The uphill would've been a big issue except we
rented powered bikes which assisted you when you pedalled, making the whole
thing quite painless.

I was actually quite scared riding around on the streets, since I hadn't ridden
a bike in more than six years, we had no helmets, it was really heavy due to the
motor so the handling was different, and sometimes we had to ride at really low
speeds amongst crowds and in small gaps to dodge pedestrians. And there was no
way that I was riding on the roads since in busier areas no one seems to do it,
and when we got to the suburbs, the cars are going quickly and all you have are
a rear reflector and a headlight! There was one particularly annoying narrow
footpath which had power poles installed at regular intervals right in the
centre of the footpath and it was seriously anxiety inducing dodging past each
one. At least it didn't have many pedestrians.

![Trying to not hit anyone and staying balanced while crossing the road sucks. I
adjusted my seat pretty low and it paid off since there were many times where I
had to stick my legs out since I was afraid of falling over.](./IMG_20190906_181834.jpg)
![We arrived too late! Bob bought a jelly tub (which happened to be the same
one that I ate a few days ago from Mega Don Quijote) from the conbini as an
offering, so we just ate it in half melted form.](./IMG_20190906_185655.jpg)

Then we rode to Chinatown and dropped off the bikes. The Chinatown in Yokohama
is apparently one of the larger ones in the world. To be honest I didn't think
that I'd like it that much, but when we actually got there, I loved it, even
though it was quite dead since I assume it's mostly busy during the day. It was
probably a mix of not having eaten Chinese food in a while and being able to
understand some of the locals in the shops.

I should've looked up the area before we got there since apparently the majority
of the population are from Guangzhou and therefore probably speak Cantonese. At
the time I didn't know, so we just let Bob speak Mandarin instead. It was
actually quite amusing: the staff of both of the shops that we went to first
spoke to us in Japanese, and after a brief awkward pause, Bob asked if they
spoke Mandarin, and everyone had a laugh and then we were able to communicate.
It was nice having someone in the group being able to speak completely naturally
to the staff and properly discuss the menu. While Elbert can speak Japanese, he
still needs to put quite a bit of thought into what he's saying, and it still
feels like there's a bit of an unexplainable cultural barrier. Being Chinese and
having been to Chinese restaurants many times before, you don't need to think as
much about how to behave. I just felt very comfortable being in a familiar
environment.

![Chinatown, Yokohama: we walked past those sesame balls and I got a bit
homesick, even though it's not like I go to yum cha regularly in Sydney.](./IMG_20190906_192529.jpg)
![One of the many gates near the edges.](./IMG_20190906_192656.jpg)
![I think this temple is open during the day.](./IMG_20190906_192928.jpg)
![One of the many streets.](./IMG_20190906_192945.jpg)
![Soup and pickles.](./IMG_20190906_194553.jpg)
![The noodles were a bit overcooked and sticky, but tasty otherwise.](./IMG_20190906_195115.jpg)
![A surprisingly light fried rice. Normally these are loaded with oil. Really
good.](./IMG_20190906_195627.jpg)
![The fried rice came as a set with this rather forgettable strange jelly.](./IMG_20190906_200807.jpg)
![Then we went to a bun shop. Average but definitely satisfied my cravings. The
green ones tasted amazing but I forget what they were.](./IMG_20190906_203013.jpg)
![Red bean filling.](./IMG_20190906_204207.jpg)
![Surprisingly, seeing a roast duck store made me think of home.](./IMG_20190906_210320.jpg)

I've read that there's a small Chinatown in Ikebukuro so I might just have to go
if I get homesick.

![What a great manhole cover.](./IMG_20190906_210405.jpg)
![Another picture of a street, with drunk Japanese businessmen in the
background.](./IMG_20190906_210412.jpg)
![Seeing all these decapitated ducks was a bit creepy.](./IMG_20190906_210547.jpg)

I had to be mindful of the time because it was a long commute and I didn't want
to miss any last trains, but we were actually pretty tired so we just went home
at 2200 after dinner.

![Motomachi-Chukagai Station, Chinatown: a big cavern-like platform area.](./IMG_20190906_211814.jpg)

![Home, Ootsuka: still good.](./IMG_20190907_014216.jpg)
![Still... okay, but I just wanted chips. I ate another delicious pear but I
forgot to take a picture.](./IMG_20190907_024127.jpg)

It took about seventy minutes to get home. I also noticed that my butt was
pretty sore from the bike seat. I read a few articles about the differences
between temples and shrines in Japan[^1], blogged, then went to bed.

##### Waking hours

1030–2845

[^1]:

  it's somewhat confusing but the gist of it is that one is Buddhist and one is
  Shinto, and the architecture and naming will differ. However, most places are
  a hybrid of both because that's just a thing that happened.
