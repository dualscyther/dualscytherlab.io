---
title: Walking over Rainbow Bridge
date: "2019-09-27"
---

I left for Shinagawa at 1300. On the train I noticed that I would pass by Meguro
station so I got off to eat at Menya Ishin, a Michelin starred ramen restaurant.

![Menya Ishin, Kamiosaki: this was a yuzu shio ramen with wontons. I thought
it was great but nothing out of the ordinary from other great ramens that
I've had, but I think that's to be expected. The wontons were pretty normal
tasting too much was a little disappointing. My body did notice how
incredibly salty the broth was.](./IMG_20190927_134709.jpg)

Shinagawa station is massive and there are plenty of high rises surrounding it.
I find that the tall buildings are more noticeable when they're less densely
packed together, otherwise you can't get a feel for just how tall they are.

![Outside Shinagawa station, Shinagawa.](./IMG_20190927_142356.jpg)

I didn't feel like shopping, seeing dolphins, or going to a temple, so I
actually didn't end up with much to do. I walked twenty minutes to Tennozu Isle
where I read that there are nice views, although they ended up being decent at
best. However, I still enjoyed the walk, breeze, and ambience.

![Shinagawa: crossing onto Tennozu Isle.](./IMG_20190927_145811.jpg)
![Bond Street, Tennozu Isle: a street mainly consisting of renovated
warehouses.](./IMG_20190927_150045.jpg)
![ ](./IMG_20190927_150234.jpg) ![ ](./IMG_20190927_150301.jpg)
![A bridge across to the next island to the north.](./IMG_20190927_150443.jpg)
![There was a lot of English everywhere, sometimes without any accompanying
Japanese. Can you even tell that this photo was taken in Tokyo?](./IMG_20190927_150603.jpg)
![Plenty of people sitting along the canal enjoying a rest. Some were even having a nap.](./IMG_20190927_150534.jpg)
![Tennozu Isle.](./IMG_20190927_150800.jpg)
![Along the eastern shore, with the view blocked by another island to the east.](./IMG_20190927_152909.jpg)
![I sat down for a rest and even fell asleep for a little bit. Very relaxing here.](./IMG_20190927_153407.jpg)
![Interesting armrests.](./IMG_20190927_160052.jpg)
![Monorail with Pokemon art.](./IMG_20190927_160415.jpg)

As I walked back towards Shinagawa station, I passed by a bike hire station and
since Rainbow Bridge was nearby, I went for it. It was only after I unlocked one
of the bikes that I realised that the screw securing the seat was missing,
meaning that it was on the lowest possible setting. Perhaps that explains why it
was the only bike that was on full charge. It didn't end up too bad but it made
things a little uncomfortable.

![Konan: I rode over a bridge, snapped this picture, and descended a fair
amount, before realising that I went the wrong way and had to turn around.
Luckily the powered bike made it pretty easy.](./IMG_20190927_162638.jpg)
![Rainbow Bridge (I think the walkway is technically called Rainbow
Promenade): on the west end of the bridge you take an elevator up to the
deck. Sadly, they don't let you ride on the bridge since it's "too narrow",
and if you have a bike you have to push it along the south side when going
eastbound, meaning I didn't get to see the Tokyo skyline although I did see
Odaiba and Tokyo Bay.](./IMG_20190927_164456.jpg)
![A model of the western tower.](./IMG_20190927_164543.jpg)
![This is the solution to getting cars to make such a drastic elevation change.](./IMG_20190927_165006.jpg)
![ ](./IMG_20190927_165158.jpg)
![I'm not very comfortable on a bike but I would feel fine going through
here. I admit the photo makes it look quite narrow. Pushing a bike for 1700m
isn't very fun although the view was great.](./IMG_20190927_165319.jpg)
![One of the walkway cutout sections had a few of these to point out landmarks.](./IMG_20190927_170247.jpg)
![ ](./IMG_20190927_170302.jpg) ![ ](./IMG_20190927_170630.jpg)
![View of Odaiba.](./IMG_20190927_170943.jpg)
![View back towards Shinagawa.](./IMG_20190927_171200.jpg)
![That's a lot of little bridges to connect to this one big bridge.](./IMG_20190927_171350.jpg)

![Odaiba Beach, Odaiba.](./IMG_20190927_172647.jpg)
![ ](./IMG_20190927_172652.jpg)

I arrived at the beach as the sun was beginning to set and I spent a while just
walking along and enjoying the view. It was beautiful watching all the lights
begin to come on.

![Odaiba Marine Park, Odaiba: further along past the beach.](./IMG_20190927_175405.jpg)
![A replica Statue of Liberty. Behind it is some crazy looking mall called
Aqua City.](./IMG_20190927_175628.jpg)
![I don't think this sign enhances the view in any way but it's there for
people to take pictures of so why not.](./IMG_20190927_180339.jpg)
![Plenty of tourists here taking pictures from a thoughtfully placed
observation deck.](./IMG_20190927_180759.jpg)
![The default Google camera settings always end up brightening the sky a bit
too much, but the buildings look nice.](./IMG_20190927_181047.jpg)

I considered walking back over bridge on the north side to get a good view of
Tokyo but.. couldn't be bothered, so I caught the train home.

![Kousagi, Ootsuka: I came back for tantanmen which Elbert has here quite
regularly. It's a little spicy and has sesame paste/peanut butter. It was
excellent.](./IMG_20190927_194808.jpg)
![Hard to resist a nap after this. I was already quite tired.](./IMG_20190927_203948.jpg)
![ ](./IMG_20190927_215608.jpg)
![Nothing special but not hard to finish either.](./IMG_20190927_215805.jpg)

##### Waking hours

1210–2720
