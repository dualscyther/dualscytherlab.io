---
title: Imperial Palace Outer Gardens
date: "2019-09-30"
---

I left the house at 1245 towards Tokyo station to explore the grounds around the
Imperial Palace.

![StandT, Marunouchi: oolong tea as part of my lunch set.](./IMG_20190930_133635.jpg)
![With some rice, salad and egg. The egg was perfectly done so that it wasn't
crusty on the other side, so it cut easily.](./IMG_20190930_134158.jpg)
![A nice view from the cafe.](./IMG_20190930_133450.jpg)

![Marunouchi: this sign wasn't here before.](./IMG_20190930_141134.jpg)
![It's a bit hard to read.](./IMG_20190930_141208.jpg)
![Lot's of people taking photos of it.](./IMG_20190930_141251.jpg)

It was a very short walk to Kokyo Gaien, one of the outer areas of the Imperial
Palace.

![ ](./IMG_20190930_142352.jpg)
![Kokyo Gaien, Chiyoda City: they built a path through this tree.](./IMG_20190930_142901.jpg)
![ ](./IMG_20190930_143045.jpg)
![Keep out at night. Why?](./IMG_20190930_143528.jpg)
![ ](./IMG_20190930_143834.jpg) ![ ](./IMG_20190930_144354.jpg)
![So many tour buses.](./IMG_20190930_144526.jpg)

The leaves here were just beginning to change colour on some trees. Walking
around was nice, the sun was out (nice for views but not as nice in parts
without shade) and the birds were chirping cheerfully. I took a small break near
the statue and sat down on some grass, but it wasn't very soft and it had lots
of leaves and twigs mixed in.

![Interesting bollards.](./IMG_20190930_152508.jpg)
![A running trail around the palace grounds.](./IMG_20190930_153037.jpg)
![ ](./IMG_20190930_153109.jpg)
![That's one big gate.](./IMG_20190930_153339.jpg)
![ ](./IMG_20190930_153643.jpg)
![Nijubashi. There are two bridges but the
other one is a bit further back.](./IMG_20190930_153818.jpg)
![ ](./IMG_20190930_154006.jpg)
![The inner grounds aren't open except on certain holidays.](./IMG_20190930_154141.jpg)
![The second bridge of the Nijubashi pair.](./IMG_20190930_154309.jpg)
![ ](./IMG_20190930_154336.jpg)
![Reminds me of the Michael Jackson lean.](./IMG_20190930_154444.jpg)
![Probably why the grass looks so perfect.](./IMG_20190930_154500.jpg)
![Another gate, this one for access into the inner palace.](./IMG_20190930_155404.jpg)
![ ](./IMG_20190930_155639.jpg)
![And another smaller gate.](./IMG_20190930_155821.jpg)
![ ](./IMG_20190930_155858.jpg) ![ ](./IMG_20190930_160215.jpg)

![Wadakura Fountain Park: on the north side of Kokyo Gaien near the Imperial
East Gardens (different to Kokyo Gaien).](./IMG_20190930_160345.jpg)
![ ](./IMG_20190930_160829.jpg) ![ ](./IMG_20190930_160850.jpg)
![There was a rest station here too with tourism information.](./IMG_20190930_161859.jpg)
![ ](./IMG_20190930_162419.jpg)
![Sadly the East Gardens closed at 1600 so I was too late. This bridge to get
from Marunouchi to the fountain area was
beautiful.](./IMG_20190930_162728.jpg)
![A red brick building.](./IMG_20190930_162947.jpg)
![The side of this building reminds me of a waffle.](./IMG_20190930_163230.jpg)

Not having much else to do, I took the train to Omotesando and walked around. I
bought some socks in Tabio, and they did free name stitching too meaning I'll
have to go back to collect my purchase. Later I had some bubble tea that I'd
walked past a few times before but never entered until my friend recommended it
to me recently. It cost 620 JPY and it was cash only so I dumped almost all of
my change and the poor cashier actually bothered to count out the 500 + 50 +
5 \* 10 + 2 \* 5 + 10 \* 1.

![KOI The, Harajuku.](./IMG_20190930_190129.jpg)
![Golden bubble milk tea. It's the best I've had in Tokyo but I think it
still lacks tea taste relative to Coco in Sydney. The milk was
good.](./IMG_20190930_191312.jpg)
![I liked the texture and size of the mini pearls, which were still big
enough to convince you to chew a bit.](./IMG_20190930_193111.jpg)

Elbert was in the area so we met up and walked 15-20 minutes to the main Fuglen
Tokyo store. By coincidence, there was a restaurant next door called Bondi Cafe.

![Fuglen Tokyo, Shibuya.](./IMG_20190930_200432.jpg)
![We've already been to the Asakusa cafe and it was great.](./IMG_20190930_200440.jpg)
![Maybe it was because it was night time, but I got less of a specialty
coffee focused vibe here. For 260 JPY the espresso was not bad and cheaper
than Sydney, but it wasn't anything special
either.](./IMG_20190930_200958.jpg)

We walked back to Shibuya and had dinner (Elbert's first meal of the day).

![Negishi, Shibuya: three types of beef tongue. One was delicious and the
other two were great.](./IMG_20190930_214937.jpg)

As we walked through the busy area just outside Shibuya station to get to the
train, I spotted a guy doing free kisses[^1]. I was tempted to go for it just to
be a stirrer but didn't for hygiene reasons and I was scared of a possible
violent reaction, although someone doing free kisses probably knows what's
coming for them anyway. We got home just past midnight.

##### Waking hours

1150–2830

[^1]: last time I was here there was a girl doing free hugs too
