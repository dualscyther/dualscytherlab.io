---
title: Ginza (including Sushi Aoki)
date: "2019-08-14"
---

I got up earlier than usual to meet Jack and Cat for lunch at Sushi Aoki in
Ginza. I've put the recap below separately, so that this section doesn't get too
bloated.

Afterwards, we went (mostly window) shopping around the area. There are so many
department stores and shopping malls that I lost track of where I took some of
my photos. Some of the names that I can remember were GINZA SIX, Ginza
Mitsukoshi, Ginza Wako, Ginza Tokyu Plaza, Ginza LOFT, and Tokyu Hands. Jack
made the observation that in Japan you get all of the international brands as
well as the huge number of quality Japanese boutique brands. It was awesome
seeing so many nice looking (usually expensive) Japanese made goods. I also
wanted coffee but most of the coffee around Ginza was "specialty" and at least
800 JPY so we gave it a pass.

![GINZA SIX: Just in case you get lost in the bathroom.](./IMG_20190814_144814.jpg)
![View of Uniqlo across the street, from the rooftop.](./IMG_20190814_150203.jpg)
![Tsujiri, GINZA SIX: Matcha, matcha, matcha...](./IMG_20190814_151143.jpg)
![I got the"No. 1 !!" option. The strength of the matcha was perfect, as was the
powder on top. Flavour was great and the texture was way denser than I've ever
had in a soft serve before, rather than the usual airiness. Would (and will)
eat again.](./IMG_20190814_151552.jpg)
![Look at all those plastic soft serves out the front.](./IMG_20190814_152653.jpg)
![Ginza Place: this car at the Nissan exhibit was huge.](./IMG_20190814_160411.jpg)
![Sony Park, Ginza: Back at the aquarium again. Look at that kid looking at that
fish.](./IMG_20190814_164914.jpg)
![Tokyu Plaza, Ginza: Found this amusing sign at a towel store.](./IMG_20190814_171438.jpg)
![Wood everywhere. The keyboard alone costs 100000 JPY.](./IMG_20190814_172146.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="head-scratcher.mp4" type="video/mp4">
  </video>
  <figcaption>
    Somewhere: playing around with a head-scratcher.
  </figcaption>
</figure>

![Tokyu Hands: Interesting gadget to keep yourself cool on a hot day.](./IMG_20190814_180545.jpg)
![Tokyu Hands, Ginza: The bottles on the top row are made in Japan and cost about double, that of
the bottles on the lower rows which aren't made in Japan.](./IMG_20190814_183916.jpg)
![Ginza LOFT: Bins shaped like coffee cups.](./IMG_20190814_192441_2.jpg)
![Siphon brewer.](./IMG_20190814_194021.jpg)
![These shops all have large coffee sections. This was about 1/3 of it.](./IMG_20190814_194031.jpg)
![These cutouts are all of the same comedian.](./IMG_20190814_194843.jpg)

After a great afternoon enjoying Ginza, we ate some yakitori at adjacent
district Yurakucho, looked around Bic Camera for some puzzles which we wanted to
buy, then went home.

![A yakitori shop under the railway in Yurakucho: edomame.](./IMG_20190814_201800.jpg)
![Eggplant with miso sauce.](./IMG_20190814_202705.jpg)
![Pork, 3 with sauce (called tare), 2 with salt.](./IMG_20190814_202915.jpg)
![Selfie.](./yakitori-selfie.jpeg)
![Chicken skin, and some other chicken that I forget. We ate most of it before I
remembered to take a picture.](./IMG_20190814_203719.jpg)

I got home at around 2200 and got to test out my new Uniqlo Blocktech Parka in
the rain on the walk home from Ootsuka station. I spent the next few hours
delving into library code trying to fix a bug where my blog's portrait images
were being squished into 4:3 instead of 3:4 aspect ratio, only to realise that
my images just had incorrect metadata caused by a buggy camera app. Ugh. Elbert
was tired so we didn't actually sleep too late.

## Sushi Aoki Ginza

Just before we went upstairs to the restaurant, I'd quickly looked up how to say
"I have a reservation", and I repeated it in my head the entire time we were in
the elevator, until I finally got to spit it out when we got inside. We were led
past the long main counter where most of the patrons were seated (and some being
served by Aoki-san himself) to the other counter where we were greeted by chef
Hatori-san.

Prior to booking, I'd looked up the restaurant and seen a 10000 JPY lunch set
which I was keen to try, since we didn't want to spend _too_ much. When
Hatori-san gave us the menu, there was no lunch set in sight, only a 15000 JPY
sushi option or a 25000 JPY sushi + otsumami[^1] option. I tried to ask about
the lunch set but I hadn't learnt the Japanese for "existence" yet, nor how to
say "ten thousand" so he mistook my words as a request to go with the 15000 JPY
sushi omakase menu option, so that's what we ended up going with. It ended up
being 12 pieces + we made an extra order at the end.

![Bonito appetiser: deep flavour like lean tuna but more
subtle.](./IMG_20190814_123748.jpg)
![Flounder: not a lot of flavour, but had an interesting texture, like jellyfish
but without the crunch. I also liked the ginger, which was served in blocks
rather than thin slices, although this probably made it more filling.
Hatori-san was diligent in refreshing the ginger when it ran low.](./IMG_20190814_125105.jpg)
![My seat was great because I got to see behind the counter.](./IMG_20190814_125622.jpg)
![The chairs were cool, they had little legs.](./little-chair.jpeg)
![Akami (lean tuna) roll: the tuna was really soft, and the flavour was quite
pronounced (in a tasty way). I felt like the rice needed a bit more seasoning
to balance it out.](./IMG_20190814_125910.jpg)
![That's a lot of little tentacles.](./squid-box.jpeg)
![Chuutoro (medium fatty tuna): so soft and melty yet without usual slightly
sickening feeling that usually accompanies eating fat. Again, I wish there was
a tiny bit more seasoning (sweetness? acidity? I'm not expert) to balance out
the taste of the tuna.](./IMG_20190814_130505.jpg)
![Squish squish.](./IMG_20190814_131015.jpg)
![Putting on the finishing touches.](./finishing-touches.jpeg)
![Where's that piece going?](./ready-to-eat.jpeg)
![Horse mackerel: not fishy at all (first time I've experienced that with
mackerel), barely slimy. Best horse mackerel I've ever tasted. Actually, just
one of the best pieces I've ever tasted. MMMM. I forgot to order this again at
the end.](./IMG_20190814_131044.jpg)
![Baby sardine: still had a very slight fishy taste that I associate with
sardines, but not bad at all. Very soft.](./IMG_20190814_131750.jpg)
![Sabrefish (swordfish? I'm not sure if he mistranslated): slight fishy taste,
interesting texture; soft but if you bite all the way down it's almost crunchy
(maybe that's the skin).](./IMG_20190814_132205.jpg)
![Ootoro (fatty tuna): nothing out of the ordinary but it's still ootoro so of
course I'm going to enjoy it](./IMG_20190814_132659.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190814_132652.mp4" type="video/mp4">
  </video>
  <figcaption>
    Hatori-san making the ootoro piece.
  </figcaption>
</figure>

![Hatori-san giving us a squid anatomy lesson.](./IMG_20190814_133741.jpg)
![Baby squid: soft, but not annoyingly chewy like I'm used to. More like the
firm al dente pasta texture. The sprinkle of salt on top and the sauce/wasabi
gave it the right balance of flavour since squid itself has quite a mild taste.](./IMG_20190814_133746.jpg)
![Shrimp, egg, shiitake roll: the egg was a bit too sweet for my taste and was
overpowered the shrimp a bit, but the mushroom was a great addition.](./IMG_20190814_134915.jpg)
![Hokkaido uni (sea urchin) - so sweet, so creamy. I can't really describe how
amazing this was.](./IMG_20190814_135232.jpg)
![This deserves two pictures.](./IMG_20190814_135234.jpg)
![Hattori-san pulled out these boxes of different uni varieties and asked if we
wanted to order them. I wasn't sure how many more pieces we were getting so we
said something along the lines of "maybe later", but in hindsight, him asking
was probably a signal that our meal was coming to an end. We were starting to
get full at that point anyway.](./IMG_20190814_135455.jpg)
![Eel (anago): sweet, really creamy, and the caramelisation on top added this
perfect mix of sweetness and slight bitterness.](./IMG_20190814_135915.jpg)
![Cute cup.](./IMG_20190814_140236.jpg)
![Miso soup to finish it off.](./IMG_20190814_140242.jpg)
![Kuruma ebi (prawn) with lime peel: Hatori-san asked us for any extra orders
that we wanted, so Jack and I asked for prawn. Unfortunately, we didn't forgot
how to say "raw" at the time, and Hatori-san didn't understand us. It might be
the tastiest cooked prawn sushi that I've eaten, but it's still cooked 😔.](./IMG_20190814_141919.jpg)

Overall, a great meal. Actually, looking back at these photos genuinely made me
salivate. This also happened to be the first Michelin-starred restaurant that
I've eaten at. Hatori-san was polite and did his best to speak English to us,
explaining what each piece of seafood was, where it came from, and giving us
both English and Japanese names[^2]. Aoki-san himself came to our counter to
thank us at some point too, which was cool.

![Hatori-san's card.](./hatori-jun-card.jpeg)
![You wouldn't be able to find this place if not for this unassuming lantern on
the ground.](./IMG_20190814_143134.jpg)

##### Waking hours

1130–2620

[^1]:

  I'm not really sure what this is but it's something like side
  dishes/snacks/appetizers to accompany alcohol

[^2]:

  Though you can see that I forgot many of the Japanese names, just look at my
  inconsistent captioning
