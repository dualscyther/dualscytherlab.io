---
title: Kyubey, Ginza
date: "2019-09-03"
---

Kyubey opens at 1130 and they take walk-ins for lunch, so I left the house at
1050 to hopefully secure myself a seat for lunch. It was raining on and off so I
stuffed my umbrella in my bumbag, leaving the handle poking out since it didn't
quite fit.

![Kyubey, Ginza: an unassuming exterior that I accidentally walked past since I
didn't really know what I was looking for.](./IMG_20190903_113658.jpg)

I forgot to rehearse a sentence to say that I didn't have a booking, so even
though I said it correctly, I took my time making it up on the spot and the lady
switched to English immediately, asking for my name and telling me to come back
at 1230.

I went to the nearby GINZA SIX building to look for a snack to open up my
appetite, but didn't really find anything except for desserts on the basement
level.

![Outside GINZA SIX, Ginza: pretty and yummy. I planned to eat it immediately
but the store wasn't really designed for people to eat the pastries there, so
one glance away and the lady had already wrapped it in a paper bag with an ice
ice pack.](./IMG_20190903_121754.jpg)

Kyubey is pretty big, the main shop ("Honten") has a few floors and there's also
a building across the road which I saw some customers get led to. On top of
that, they also have heaps of shops around Japan so it's essentially a high end
sushi chain. A few blog posts that I've read have recommended Kyubey as the
place to go for \$\$\$ sushi first timers, because it's not too pricey
(relatively speaking of course), the sushi is obviously good, and the sushi is
plain/normal/vanilla so an unaccustomed palette will be more likely to enjoy it
compared to somewhere like Sukiyabashi Jiro where the rice is incredibly
vinegary.

I was taken up to the fourth floor (the first floor is the ground floor in
Japan) and came out into a waiting room. There were sake bottles and plates in
display cabinets, as well as a few nice information displays that I presume to
be about the history of the place and the chefs. Just as the room was getting a
bit too full, a lady came to pick up a few of us and took us to the second
floor, this time to actually eat. The lunch menu had a few options, a few of
which were:

- 10 pieces for 5500 JPY (reduced from 7500 JPY, there's an ongoing 2000 JPY
  discount on every menu item to celebrate their anniversary or something)
- 12/13 pieces for 7500/8500 JPY (sorry my memory is fuzzy)
- 16 pieces for ~15000 JPY. I've found that the price usually doesn't scale
  linearly with the number of pieces on these menus, probably because you get
  more expensive fish (or ones that take more effort to prepare) and I suppose
  you take longer to eat too. I forget, but I think you might also get some
  otsumami (essentially additional dishes).

I chose the ten piece option because I wasn't sure how many pieces I could eat,
and I was looking out for my wallet a little bit.

![Kyubey, Ginza: The soup had a strange slimy texture, it was fine but I also
wasn't wowed or anything. It was also a bit strange drinking out of a square
cup. The side was some yummy pickles, and you could get unlimited refills. When
the chef asked if I wanted a refill, I declined because I wasn't sure if I'd be
able to finish it and the rest of the meal.](./IMG_20190903_124113.jpg)
![I can never complain about chuutoro, although I feel like I hadn't entered
sushi appreciation mode at this point so I didn't savour it as much as I
should've. The tuna flavour was well balanced with the rice.](./IMG_20190903_124215.jpg)
![Halibut. The texture was interesting, a little chewy like jellyfish (I've used
this description before because it's my closest approximation, even though it's
not really that accurate). Had some flavour but not a crazy amount.](./IMG_20190903_124459.jpg)

I forgot to take a photo of the squid again. It had salt and sudachi (a Japanese
citrus) juice on it. It was really good but my notes don't say whether I loved
it or just liked it.

![Someone delivered the live prawn to my chef and he asked me if I wanted it
cooked or raw. I said "nama" (fresh/raw) and he was about to rip its head off
but I asked him if I could take a photo, so he put it back down.](./IMG_20190903_124550.jpg)
![This uni was amazing. Creamy and sweet. I don't have much of a vocabulary to
describe the subtleties of uni flavour and texture. I also keep reading that
the founder of Kyubey invented/popularised this style of sushi called "gunkan
sushi", named for the way its appearance resembles that of a battleship.](./IMG_20190903_124818.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190903_124707.mp4" type="video/mp4">
  </video>
  <figcaption>
    I was just taking a video to show how the prawn was still moving its mouth
    but then this happened.
  </figcaption>
</figure>

![I had the choice of having my prawn with salt or sauce so I asked the chef for
what he thought and he said salt. Good, but again, prawn isn't my favourite
neta.](./IMG_20190903_124933.jpg)
![Ootoro. Obviously very fatty, and also had a little bit of sinew which I'm
personally not a fan of. I liked that I could actually taste the tuna rather
than some ootoro which just tastes like fat, although for some reason I found
myself wishing that the fish was a bit warmer to match the good temperature of
the shari. Also I like how this piece was presented.](./IMG_20190903_125505.jpg)
![Scallop which wasn't superbly sweet, but rather had some noticeable pleasant
and subtle flavours instead. Actually now that I think about it, it reminded me
of the taste of cooked scallop, and it also wasn't that slimy. I wonder whether
that's because it's been slightly cooked, some other preparation, or just the
species of scallop.](./IMG_20190903_125721.jpg)
![Katsuo, which the chef translated as skipjack tuna but is also often
translated to bonito, a related but different fish. He asked me if I was okay
with garlic and I said yes, although I'm pretty sure what I ate was ginger since
it tasted like it, and I overhead another chef translating it as ginger.](./IMG_20190903_125832.jpg)
![Anago (eel). Given as one half with sauce, and the other with salt. They
didn't blow my mind but I liked both.](./IMG_20190903_130211.jpg)
![Hand rolls, which I was instructed to dip into my soy sauce. They were lighter
than I'm used to, so that's a win, although I'm still not a big fan of them
relative to the nigiri pieces. I understand that it's pretty standard though.](./IMG_20190903_130520.jpg)
![Tamago, which I could choose to have with or without rice. I chose with rice.
It was steaming hot, hotter than I've had anywhere else, and I liked it. It
reminded me of Chinese egg tarts. The eggplant on the right was not bad too,
pickled and somewhat salty.](./IMG_20190903_130601.jpg)

At this point my chef smiled, crossed his arms and swept them aside,
emphatically declaring "finished" to me and the Chinese couple beside me. I
don't think they understood because he had to remind them again later by
gesturing in the direction of the exit to them.

![I was sat at a long counter with four chefs, here are three of them (along
with one of the many assistants who constantly come in and out to give eel,
shrimp, soup, and other things, to the chefs). Another one was on my left. It
was interesting that everyone ate off of one long shared plate.](./IMG_20190903_131241.jpg)
![I'd read that kohada (konoshiro gizzard shad) is a sushi classic, and a good
way to gauge the ability of the restaurant since it takes a lot of preparation
and can spoil easily, but is supposed to be amazing when it's done right. I'm
pretty sure that I've only had this fish for sushi one other time, so needless
to say I haven't quite acquired a taste for it since it's one of those sardine
family fish. I didn't find this piece to be anything noteworthy, although it
wasn't bad either.](./IMG_20190903_131546.jpg)

My chef offered me more pickle after my extra piece but I declined since I was
full at that point.

![Deep fried prawn tail and head, presumably from the same prawn that I'd eaten
earlier. A surprise to be sure, but a welcome one.](./IMG_20190903_131617.jpg)
![I left my miso soup to cool down and drank it at the end. This time I didn't
spill it. I found the clams awkward to remove from the shell so I just gave up
two thirds of the way through since there wasn't much payoff anyway.](./IMG_20190903_131915.jpg)

I enjoyed my experience. The atmosphere was lively but relaxed, which is far
less intimidating than a small, quiet counter with a chef that can't speak too
much English. Most of the customers in the room were tourists, although I'm not
sure if this is indicative of the patron demographics, a coincidence, or whether
the staff seat the foreigners together (which I've read is extremely common).

An older, rather funny chef with not horrible English speaking ability was on my
left, and he was quite the conversationalist with his customers. He was serving
an American (Asian heritage) couple who were eating high end sushi for the first
time, and he explained how to eat properly, what he was putting on the fish, and
was happy to answer their questions in detail. At one point he grew alarmed when
he saw the guy dipping his sushi into the soy sauce and had to tell him not to.

He also served a Spanish speaking group of one younger guy fluent in Japanese
with four elderly friends/relatives. I was super amazed by his Japanese, and I
could see that the Asian couple were too. One thing I found amusing was that the
old chef kept replying in English to the older relatives, before eventually the
younger bloke told him that his relatives couldn't understand English. That was
when I overhead that they were visiting from Barcelona, although I assume that
he himself was living in Japan.

My chef wasn't as talkative–perhaps because he was younger, his personality, or
worse English–but he did know how to say "bao la?" in jest to the couple beside
me. The meal came to 6588 JPY (5500 + 600 kohada order + tax). I was satisfied
and I knew that I was eventually going to come to Kyubey at some point, but you
really do notice the diminishing returns when you compare it to somewhere like
lunch at Sushi Kidoguchi which is an absolute beacon of value.

![Toriba Coffee, Ginza: I spotted this one block down from Kyubey, and saw
that they had 100 JPY espresso/drip and 300 JPY cappuccino. Such a low price
would be a red flag but they also had a roastery upstairs so I thought that
it couldn't be that bad.](./IMG_20190903_150405.jpg)
![There was some bean residue in the coffee which is always annoying. Not bad as
far as dark roasts go, I could definitely taste flavours other than bitterness.
It had quite a thin body. The place seems to be a bit more classic Italian
rather than hip third wave style.](./IMG_20190903_150016.jpg)

Then I walked to another coffee shop which I'd spotted on my map nearby. I was
at the door wondering whether I should go in, when a stranger (Japanese) came
out and told me it was good, said something about Sapporo (based there or
roasted there I don't really know) and briefly explained the menu which I
couldn't read. My looks must pass for a Japanese person because everyone speaks
to me in Japanese if I haven't opened my mouth yet. Or I suppose that's just the
language which someone would default to when in Japan...

The menu was pretty big so I asked for recommendations, and was told to go with
their regular blend. I had three size options to choose from: regular, smaller,
or demitasse. I chose demitasse thinking that it would be about the same
strength as the last traditional coffee shop (kissaten) that I went to, but boy
was I wrong. I suppose that's why the waitress warned me that it would be very
strong. I probably should've just taken the regular size. The coffee was a
traditional Japanese pourover, bag filter, stovetop kettle and all. I added the
provided cream just to make it taste a bit less strong, although it probably
dulled out the flavours a bit too. Maybe it was because it was a blend, but I
couldn't taste any distinct flavours other than bitterness (not that it was
purely bitter, I just couldn't pick out anything). It was probably roasted a bit
dark too.

![Miyakoshiya Coffee: the cheesecake was great. The coffee was okay but I
wasn't the biggest fan. Talk about inversion of prices, the cheesecake was
540 JPY and the coffee was 810 JPY!](./IMG_20190903_152837.jpg)
![Since I'd already experienced the kissaten atmosphere and coffee style, I
can't say that this visit was particularly worth it.](./IMG_20190903_154125.jpg)

I spent a bit of time window shopping again, particularly looking at watches and
watch straps, which I had zero interest in until I tagged along with Jack the
other week. I also briefly looked for wool detergent and asked a shop assistant
for "uru" (the Google translation) detergent, but it turns out that they
actually have a word for it here called "ke" which translates to fur. Anyway, it
was all a bit silly because I realised that someone has probably written a guide
for this in English, so I looked it up and found many. I'll try to buy some
soon.

I walked towards Yurakucho Station to go home, but first, the small Godzilla
statue.

![Gojira.](./IMG_20190903_173207.jpg)
![I wore a polo today since it wasn't that hot and I was going to be eating at
a nice restaurant. I don't think that a busy place like Kyubey is going to care
but I've (only recently) read that some places prefer that you don't look like
a complete slob, which I now realise makes sense considering how much it costs
to eat at one of these places.](./IMG_20190903_173307.jpg)

![Home, Ootsuka: carbonara from FamilyMart. Not bad, but easily my least
favourite of the conbini pasta flavours that I've tried so
far.](./IMG_20190903_204118.jpg)
![On the other hand, I bought two of these and saved one for a rainy day.
Absolutely amazing.](./IMG_20190903_223255.jpg)

I don't feel like I did much today except for Ginbura (an old local slang for
walking around Ginza) and visiting Kyubey. Where did my time go?

##### Waking hours

1030–2830
