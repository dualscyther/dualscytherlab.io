---
title: Accidentally catching an idol show
date: "2019-09-01"
---

![Home, Ootsuka: a rather large unidentified flying bug on my t-shirt. I hope
it didn't lay eggs.](./IMG_20190901_155952.jpg)

We went to get food at Ikebukuro. It wasn't excruciatingly hot so we decided to
walk for twenty minutes rather than catching the train.

![Somewhere in Ootsuka: we were super hungry and it was already 1700 so we
bought a snack before breakfast.](./IMG_20190901_171150.jpg)

We walked into Sunshine City and saw an idol event going on. We weren't supposed
to take pictures[^1] but I saw someone else doing it, which gave me the courage
to do it too. Firstly there was a solo idol, and once she finished her show, a
group came on. It was crazy how the fans knew the moves and timing to both of
the shows. The solo idol was making jokes and the crowd was laughing so I asked
Elbert "is she that funny or do they just laugh at her jokes" and he said "what
do you think?"

![Sunshine City, Ikebukuro: I couldn't see anything here since all the spots
were taken, so we went up two more floors.](./IMG_20190901_181143.jpg)
![This view was awesome because I got to see the choreography and the insane
crowd.](./IMG_20190901_181859.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190901_182628x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    The fans knew exactly when to sing along (super loudly), when to clap, when
    to mirror the idols' movements (like matching their hand hearts), when to
    jump up and down, etc. It was more exciting to watch than the idols
    themselves.
  </figcaption>
</figure>

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190901_182802x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Even fans not in the main group were participating. Some were alone, one
    came prepared and brought binoculars, and this little group up the top had
    their own thing going on.
  </figcaption>
</figure>

![Setting up for autographs after the show.](./IMG_20190901_183737.jpg)

The show was over so we looked around the food court. There was an omurice place
that looked good but it had a decent line, so we got Italian instead.

![A restaurant in the food court, Sunshine City: I've been enjoying the pasta in
Tokyo. This had eggplant and an incredible amount of mozzarella.](./IMG_20190901_185740.jpg)
![Elbert ate a Hamburg steak.](./IMG_20190901_185747.jpg)
![Tiramisu for dessert (where are the layers?). Not bad at all. This spoon was
weird and its shape allowed it to somehow get stuck in my teeth as I was pulling
it out.](./IMG_20190901_191901.jpg)
![Still there signing after our meal.](./IMG_20190901_193617.jpg)

We went downstairs to get closer to the idols and fans (and maybe get to say hi)
but just as we got there, they started leaving. They were waving to everyone as
they walked off the stage so I decided to be silly and did as the fans did,
waving frantically to try to get a wave back from any of the idols. _Maybe_ one
or two of them looked at me and waved back. I even noticed a small group of fans
off to the side of the stage, who were getting waves from all of the idols, so I
frantically ran towards them to get in on the action, but I was too late.

After some watch window shopping at Bic Camera[^2] we got home at 2120. I
watched the F1 Belgian Grand Prix and blogged before going to bed.

![Home, Ootsuka: a decent (not amazing) ice cream sandwich from FamilyMart.](./IMG_20190901_211706.jpg)
![Elbert's being making ham and mayo sandwiches recently for late night snacks.
This time I was hungry so I had one too. The bread is soft and the mayo is
great. Frugal and tasty.](./IMG_20190902_014722.jpg)

##### Waking hours

1505–2800

[^1]:

  I wonder why? Tight control of their image/media? Distractions? They want you
  to pay for pictures? Who knows.

[^2]:

  it's insane, there are five stores in Ikebukuro alone. We only went to two.
