---
title: I finally ate omurice
date: "2019-09-04"
---

I left the house at 1500 with the vague plan of finding a cafe somewhere and
sitting down to learn Japanese. I didn't take my laptop but I did fit my
headphones snugly into my bumbag.

![Mos Burger, Ootsuka: my friend Wilton recommended that I get the prawn katsu
burger. It did not disappoint.](./IMG_20190904_151615.jpg)

I wanted to buy some good shorts that had:

- secure pockets
- that I could wear casually but not look like a bum at a restaurant (so nothing
  that looks particularly active)
- that would be breathable and feel great, particularly on really hot days
- that would last

so I went into a few outdoor brand outlets like Arc'teryx and Patagonia but the
range that they had in-store didn't match their global websites. It must've been
the coolest day in a while, apparently it was 27C but to me it felt more like
24, on top of a cool breeze.

I made my way towards Shibuya to a cafe that I'd marked on my map, planning to
enjoy the WiFi and learn some Japanese on my phone.

![Streamer Coffee Company, Shibuya: this place is apparently popular in part due
to its latte art.](./IMG_20190904_175741.jpg)
![The art was great, but I don't
think the milk was noteworthy and there was definitely far too much of it. No
wonder they also offer double or triple shot versions. The cup annoyed me
because it's such a huge cup that's heavy when full, but the handle
encourages you to hold it by one finger and
thumb.](./IMG_20190904_171602.jpg)

Unfortunately it closed at 1800 so I only got about forty five minutes in before
I had to get up and go somewhere else. Luckily, Elbert messaged me and asked if
I wanted to eat omurice in Shibuya, so I just needed to kill half an hour by
going to a Montbell outlet to window shop and look for shorts.

![Pomme no Ki, Shibuya: complimentary miso soup.](./IMG_20190904_194932.jpg)
![Mushroom tomato omurice. I didn't really have any expectations.](./IMG_20190904_200408.jpg)
![Yummy. It's essentially the same as tomato rice mixed with scrambled egg, and
that makes it delicious.](./IMG_20190904_200745.jpg)

Jack had told me to check out the Mega Don Quijote in Shibuya for cheap fresh
food, so we went there before heading home. I managed to pick up baby tomatoes,
pears, grapes, fruit jelly, milk, and even detergent; although we couldn't find
one with minimal additives, at least it said that it was suitable for wool and
had no softener.

![Mega Don Quijote, Shibuya: I didn't buy this but look at those individually
wrapped tomatoes.](./IMG_20190904_214211.jpg)
![Pineapple, apricot, and mandarin jelly. The fruit taste wasn't that
noticeable but at least they had texture, and the jelly flavour was good
anyway.](./IMG_20190905_000839.jpg)
![I wonder if every milk brand here is this tasty and fatty. So good.](./IMG_20190905_044833.jpg)

I stayed up blogging so I slept late.

##### Waking hours

1310–2900
