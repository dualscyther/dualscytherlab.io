---
title: Tokyo Midtown
date: "2019-08-23"
---

We got up earlier than usual and left at 1300 because I thought that I shouldn't
waste my _entire_ holiday away, and I didn't care too much about the tournament
games being played today. The only plan today was to go to a specific coffee
shop in Shimokitazawa, but we ended up making a whole day out of it.

Firstly we ate lunch....

![City Country City, Shimokitazawa: the pasta that I've had in Japan so far has
been pretty good, so why not.](./IMG_20190823_134917.jpg)
![The place has a messy Newtown look. These are all records which you can buy.
I assume the restaurant plays lots of different music, but at the time they
were playing some great Brazilian boogie funk.](./IMG_20190823_135551.jpg)
!["Complimentary" drink. I ordered apple juice, Elbert got jasmine tea.](./IMG_20190823_135627.jpg)
![You can listen to the records on the player before you buy them.](./IMG_20190823_135718.jpg)
![Here's another picture.](./IMG_20190823_142811.jpg)
![I ordered a spaghetti carbonara. It was a bit heavy and the pasta wasn't
bouncy, it was more like the texture of cooked supermarket pasta with some
starchy aftertaste that I associate with raw pasta (it wasn't raw, that's just
the only way that I can describe it).](./IMG_20190823_140330.jpg)
![Elbert's pasta had chicken and summer vegetables. The chicken looks sort of
raw for some reason, but it wasn't.](./IMG_20190823_141129.jpg)

Then we got coffee at FRANKIE Melbourne Espresso. It was a small place that
mainly did coffee–the best kind of coffee shop. I ordered in Japanese but the
lady immediately replied in English in a minimal accent, "do you want to have
here or takeaway?" Maybe she heard me speaking in English to Elbert, or maybe it
was my pronunciation of flat white, or maybe it was because most people who
order flat whites are Aussies. It seemed like she had some internal estimate of
my Japanese ability and was being very thoughtful, because she told me how much
to pay in Japanese but then said "cash only" in English when I tried to pay by
card. Not that I can ever catch what they say when they tell me the price,
because they say it so damn quickly.

![FRANKIE Melbourne Espresso, Shimokitazawa: look at the kangaroo in the corner!](./IMG_20190823_145533.jpg)
![In hindsight I should've tried a matcha lamington or an Anzac biscuit. They
also had a bunch of other typical Aussie cafe food like banana bread, lemon
slice, and brownies.](./IMG_20190823_145506.jpg)
![Elbert's chai latte. Pour when the timer runs out.](./IMG_20190823_145240.jpg)
![It's like I never left Sydney. They line the walls with beans and the same
kind of keep cups](./IMG_20190823_145339.jpg)
![I haven't had a milk coffee in a while, but I had to get a flat white since I
don't think that you'll find many of them (at least by name) in Japan. It was
amazing, it had a distinctively high umami taste and the milk must've been
been really fat or something; sooo good.](./IMG_20190823_145455.jpg)

Then we walked back to the station and stood around deciding what to do next. I
had thought about going to a park or garden and walking around, but it had
started raining and we didn't bring an umbrella since the forecast had said that
it was only a 30% chance. Not that it's nice to walk around in the rain anyway
even with an umbrella. By some strange coincidence, a young Japanese + Aussie
(the guy was Aussie) couple had come to City Country City while we were there,
then also went to FRANKIE Melbourne Espresso while we were there, then walked
past the station while we were there too. Since we were out of things to do, we
jokingly said that it was our turn to follow them.

We eventually decided to take the train to Nogizaka station to see the National
Art Center since it wasn't too far, the architecture looked cool, and the
exhibits might be cool too. All I knew was that they rotated their exhibits all
the time and that it costs ~1500 JPY, but for some reason I didn't think to
actually check what was showing–which was a mistake because one was Japanese
calligraphy which I wouldn't be able to read for 500 JPY, and the other was
something else that didn't look too interesting for 1600 JPY. We ended up going
into the art library on the top floor and I read a book about a manga, its
history, and its cultural significance.

![National Art Center, Roppongi: we came through this back entrance from the
station. I liked the stairs.](./IMG_20190823_160937.jpg)
![The view down the main hall.](./IMG_20190823_160953.jpg)
![A restaurant sitting on top of one of those huge concrete cones.](./IMG_20190823_162010.jpg)
![Interesting that this guy's furniture is marketed to museums or something, and
also that they decided to put up this information about it.](./IMG_20190823_162734.jpg)
![Elbert enjoying his chair I think? I didn't find it particularly comfy.](./IMG_20190823_162636.jpg)
![Cool.](./IMG_20190823_163926.jpg)
![The art library. No pictures allowed inside, but it's really just a library.](./IMG_20190823_175713.jpg)
![This is a different cone, but also has a restaurant on top. You can see the
smaller cone (that you saw before) behind it.](./IMG_20190823_175835.jpg)
![Outside the front entrance. You know you're near Roppongi if you can see the
Roppongi Hills Mori Tower.](./IMG_20190823_180221.jpg)
![This is what it looks like from the front.](./IMG_20190823_180316.jpg)
![The sliding doors at the front entrance, which meet at an angle. I went back
for a picture of this.](./IMG_20190823_180642.jpg)

We saw that Tokyo Midtown was a two minute walk away so we headed there just to
check it out. You really can't run out of places to window shop in Tokyo.

![Tokyo Midtown, Akasaka: every building that you see here is part of the
complex.](./IMG_20190823_181027.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190823_181504.mp4" type="video/mp4">
  </video>
  <figcaption>
    Cool water feature.
  </figcaption>
</figure>

![The interior of the main shopping building.](./IMG_20190823_181634.jpg)
![Looking out to the garden.](./IMG_20190823_181808.jpg)
![We walked past a dog getting its haircut and standing completely motionless.
We weren't sure whether it was a real dog at first. Actually I'm still not sure.](./IMG_20190823_182055.jpg)
![We walked past this so we gave it a try since we had nothing better to do.
This photo was taken after the next three, which is why it's dark here, since
the sun was beginning to set when we arrived.](./IMG_20190823_184442.jpg)
![Somewhat busy. This would be a nice date spot. The cicadas were also chirping
so loudly.](./IMG_20190823_182722.jpg)
![Dipping our feet in. It was actually pretty cool outside, so this would've
been great on a hotter day. We didn't buy towels so we just let our feet mostly
dry naturally before wiping off with a pocket tissue.](./IMG_20190823_182827.jpg)
![I forget what this plastic is called, but it does the job I guess.](./IMG_20190823_182851.jpg)
![Tatami to sit on.](./IMG_20190823_184607.jpg)
![A nice looking outdoor bar.](./IMG_20190823_184746.jpg)
![I wonder why, maybe it gets harder to produce mist or something? Or do they
mean that they spray mist until the sensors detect 75% humidity?](./IMG_20190823_184918.jpg)
!["Stop taking photos." - Elbert probably.](./IMG_20190823_184926.jpg)
![It actually got more misty than this, so that you couldn't see the ground.](./IMG_20190823_185051.jpg)
![Is this Vivid Sydney or something?](./IMG_20190823_185445.jpg)
![We actually saw quite a few people wearing yukatas.](./IMG_20190823_185531.jpg)
![They played more upbeat music when the lights started dancing.](./IMG_20190823_190110.jpg)
![Tokyo Midtown buildings in the background, and people watching from a balcony.](./IMG_20190823_190321.jpg)
![There's Tokyo Tower.](./IMG_20190823_191209.jpg)
![Out the front of the complex on the main street. There's the Mori Tower again.](./IMG_20190823_191800.jpg)
![The front view of Tokyo Midtown. I think these are all office buildings.](./IMG_20190823_191834.jpg)
![The left one is the Fujifilm headquarters.](./IMG_20190823_191919.jpg)
![The right one is the Konami headquarters.](./IMG_20190823_191944.jpg)

We were hungry so we browsed the food level and found something to eat. I also
went to use the urinal and I keep being surprised by how they flush not only
after you're done, but before you go.

![Some restaurant, Tokyo Midtown: dashi chazuke. You submerge your rice in soup
(from the teapot). Delicious, light meal.](./IMG_20190823_194101.jpg)
![I didn't want this ice cream enough to buy it, but sake tasting ice cream
sounds interesting.](./IMG_20190823_200032.jpg)
![That's a lot of Pikachus.](./IMG_20190823_200405.jpg)
![(Expensive) fruit store. Looks somewhat fancy. I took this photo quickly
because the man was just standing there looking outside most of the time and I
wasn't brave enough to just take it in front of him.](./IMG_20190823_201038.jpg)

Making our way back home, we were about to get on the train when I saw "Ginza"
as one of the stops so I had second thoughts about whether we were taking the
train in the right direction. We looked at the nearby stops map together and
confirmed that it was the right train, so I ran onto the train, but then the
doors closed before Elbert could make it on. Oops.

![Home, Ootsuka: pudding bought from 7-Eleven on the way home. I put it in the
freezer when we got home to make it a bit cooler, but then forgot about it so it
partially froze. It still tasted good although I was impatient so I didn't let
it completely thaw. ](./IMG_20190824_012707.jpg)
![The sauce wasn't as good as yesterday's, and I think even if it was fully
unfrozen it still wouldn't be nearly as creamy (I think yesterday's pudding was
a gem in a sea of mediocrity).](./IMG_20190824_013012.jpg)
![I also bought some Calpis since I've only tried Calpis Water before. It tastes
similar to Yakult.](./IMG_20190824_021526.jpg)

I bet Elbert that I'd be in bed by midnight, but then failed, so I ended up
staying up late to write an extra blog post.

##### Waking hours

1200–2905
