---
title: Shopping successful
date: "2019-09-20"
---

Today I was determined to actually buy things rather than leave them behind in
the basket. I left the house at 1425 and caught the train to Ikebukuro station.
I realised that I'd never actually walked around right above the station so I
went upstairs to eat and realised that it was a massive department store
(nothing out of the ordinary for station buildings in Tokyo) called Seibu.

![On the food level of Seibu, Ikebukuro: eel rice. The Japanese menu outside
looked like there were different varieties of eel with different costs, so I
was sad that I would probably have to look out for my wallet. However, the
English menu clarified that they were just different sizes of food, so I
guess they just used unconventional names to indicate the sizes. If you look
closely you can see the pepper on the eel, which the waitress kindly
sprinkled on for me after I asked what I was supposed to do. The eel was
amaaaaazing, it melted in my mouth every bite. I was also surprised again by
how the staff were in kimonos. I guess that's just a thing at eel
restaurants.](./IMG_20190920_150043.jpg)
![That black bottle holds the pepper, with a stopper on the top. Seems like
form over function to me.](./IMG_20190920_150045.jpg)
![Seibu, Ikebukuro: I can see this being useful if you're desperate and it's
a busy time.](./IMG_20190920_153403.jpg)

![Loft, Seibu in Ikebukuro: there was a six level Loft which took half of the
floorspace on each level. When I asked someone where the Platinum branded
pens were, he took me to a section of the store that looked like a jewellery
store for pens. There were ballpoints and fountain pens in well illuminated
glass cases. This cabinet wasn't that expensive but I did like the look of it
and all the colours. I also saw a staff member polishing/rubbing/maintaining
some of the more expensive leather binders which were in the 5000-30000 JPY
range.](./IMG_20190920_171103.jpg)

In total I think I spent three hours eating and walking around inside Seibu,
getting out of that maze at 1720.

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190920_172012x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Ikebukuro: someone was singing outside and there were some women that
    were very into it. Maybe it was a popular song?
  </figcaption>
</figure>

I finally managed to buy my things at Bic Camera. The man who I'd talked to last
night, remembered me and helped me out. Like at Don Quijote, staff at the
checkout were Chinese too, which really made sense when you looked around and
realised that most of the shoppers on the watch floor were Chinese tourists.

My cashier experience was actually pretty poor. One of my items had to be
swapped out behind the counter for the real thing, but only on the cosmetics
level five floors below. It's obviously a hassle, but the lady blatantly acted
unimpressed and inconvenienced. The total eventually came to less than 30000
JPY, which I didn't expect because I'd actually been keeping track on my phone,
so I ran downstairs to grab another puzzle, of which I'd already bought quite a
few, but I didn't mind since I didn't really know how many to get in the first
place. It turns out that they'd forgotten to scan the watch strap that I'd
bought, since it was being changed on my watch at the time. Since she'd already
processed the tax free transaction for everything else, she told me "no tax
free".

I was a little confused since I thought maybe she was saying that it wasn't
permitted since it was immediately changed onto my watch and therefore had been
"consumed" in Japan. After standing my ground, she relented, which really
annoyed me because it seemed to indicate that she really was just being lazy...

![Coffee Valley, Ikebukuro: Brazil light roast.](./IMG_20190920_193012.jpg)

I spent a while in Coffee Valley learning Japanese and people watching. A few
tables away from me was a German guy talking to a Japanese girl, having a
conversation that switched between Japanese, English, and German! Neither were
fluent in their non-native languages and they had to look things up, but they
could clearly communicate. The Japanese girl didn't seem to have much of a
Japanese accent when speaking English and German, although the German guy's
accent was very heavy (and I loved it). I've never heard Japanese with a German
accent before.

![New leather strap.](./IMG_20190920_193849.jpg)

![Kousagi, Ootsuka: eating at a new local ramen shop which I'd been
accidentally ignoring the whole time. I love ramen egg. It was empty when I
walked in at 2130 and then ten minutes later it was full. Quite
strange.](./IMG_20190920_213048.jpg)

I got home at 2200 and tried to pack away some of my shopping, although I don't
think everything is going to end up fitting in my medium sized suitcase once I
buy even more stuff.

![Home, Ootsuka: nine new puzzles to add to my collection at home.](./IMG_20190920_222633.jpg)
![And a new watch.](./IMG_20190921_002547.jpg)
![Eating some of the goods that I bought from Don Quijote. This is konnyaku
jelly.](./IMG_20190921_002301.jpg)
![I only knew that this was gummies of some sort with bursty centres.](./IMG_20190921_012130.jpg)
![They didn't taste great.](./IMG_20190921_012227.jpg)

My ankles were still a little sore from walking around all day yesterday in my
sandals.

##### Waking hours

1210–2755
