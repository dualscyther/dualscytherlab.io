---
title: Watching Dota 2 all day again
date: "2019-08-24"
---

With the help of an alarm, I woke up before noon to watch Vici Gaming lose to
Team Secret in the lower bracket of TI 😔. I went out and bought some food from
Lawson at 1700, then kept watching TI until the the tournament ended for the
day.

![Home, Ootsuka: this don was delicious. The soft eggs are always great too.](./IMG_20190824_171255.jpg)
![The don and this sandwich were both 100 JPY off. Elbert ate this.](./IMG_20190824_171055.jpg)

We left the house at 2115 to find dinner around north Ootsuka. We walked past a
takoyaki place which we thought we might go to afterwards, but we ended up
eating so much for dinner that we decided to try it another day instead.

![Ootsuka somewhere: tsukemen (dipping ramen). I thought that the dipping sauce
would be like that of cold soba or the chazuke from yesterday, but it was
actually a strong, heavy broth. Which was fine, but I didn't find the noodles
to be great, and there were so many noodles that I didn't eat all of them. It
felt bad not to eat the whole thing since it was just a small shop. After you're
done, you ask for them to dilute the broth with hot water so that you can drink
it as soup.](./IMG_20190824_213723.jpg)
![There was a timer and a kaleidoscope to play with.](./IMG_20190824_220839.jpg)

As we were walking home, Elbert tried to teach me the Japanese "u" sound since
it turns out that I'd been saying it wrong the entire time, but I just couldn't
get it. It was somewhat disheartening, since it's one thing to be able to
recognise when you're making mistakes and self correct, but when you don't know
that you're wrong then you have no feedback loop at all.

![Home, Ootsuka: Elbert taught me that your mouth is used to make sounds or
something.](./mouthdiagram.jpg)

I'm finding that learning how to make the sounds properly and having proper
intonation (the pitch) of each syllable is really difficult. When you focus on
one thing, you forget to focus on everything else. It's like learning the
technique of a movement in sport, like a golf swing or a rowing stroke.

Some mix of high carb dinner and having too little sleep made me feel really
lethargic and I napped until midnight. I woke up, pumped out a blog post, then
went back to sleep early-ish since I wanted to wake up the next day to watch the
TI finals and I may as well try to sleep earlier anyway.

##### Waking hours

1120–2610
