---
title: Japanese bubble tea is expensive
date: "2019-08-16"
---

Elbert and I left the house at 1700 to eat at Gagana Ramen, where his Japanese
friend was working. Afterwards, while we were looking at the menu on the outside
of a bubble tea shop, some huge bug flew in and started buzzing around one of
the store lights. It was pretty amusing observing the people inside react,
although perhaps we should've done something. At some point, the staff even
turned off some of the lights to try to get the bug to fly off. One of the
customers eventually trapped it in a plastic bag and released it outside.

![Home, Ootsuka: holding my freshly washed towel.](./IMG_20190816_163742.jpg)
![Ikebukuro: tempting.](./IMG_20190816_173141.jpg)
![Gagana Ramen, Ikebukuro: the slightly charred pork was great.](./IMG_20190816_174908.jpg)
![The pork innards (bottom) just tasted like fat.](./IMG_20190816_174957.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="./VID_20190816_183257.mp4" type="video/mp4">
  </video>
  <figcaption>
    Tapista, Ikebukuro: the bug whisperer. I only managed to film the tail end.
  </figcaption>
</figure>

![Large peach tea with almond jelly and a cool gradient. This cost
732 JPY and looked pretty cool but I wouldn't say that it tasted particularly
good.](./IMG_20190816_184625.jpg)

At the supermarket on the way home, the lady at the counter asked me something
as she was scanning the three desserts that I'd bought. I assumed that she was
asking if I wanted cutlery or if I had a loyalty card or something else
unimportant–since there was enough stuff that I thought there was a low chance
that she was asking me if I wanted a plastic bag–so I said no. Turns out that
she was actually asking me if I wanted a bag, and I must've looked like I was
struggling as I was trying to pick everything up, so she even asked me if I was
okay ("daijoubu") and I just said that I was since I had no idea how I was going
to ask for a plastic bag. We went home and I watched a bit of the
[Dota 2 International](<https://en.wikipedia.org/wiki/The_International_(Dota_2)>)
which has a 33 million USD prize pool this year.

![Home, Ootsuka: some desserts for later. Pudding, daifuku, and some strange
rice ball pudding thing.](./IMG_20190816_203738.jpg)
![The mochi was nice but I wasn't really a fan of the filling. Maybe I just
prefer ice cream.](./IMG_20190816_204819.jpg)
![I have no idea what to call this.](./IMG_20190816_232220.jpg)
![It took some effort not to get this (not sure what) powder everywhere.](./IMG_20190816_232542.jpg)
![A bit too starchy without enough flavour, and I definitely added too much
powder.](./IMG_20190816_233005.jpg)

We went out at around 0200 for some soba. There were actually quite a few people
at the park next to the station just sitting or napping there. My guess is that
they were waiting for the first train in the morning. The soba was quite a bit
of food since I'd just eaten some of the desserts.

![Fujisoba, Ootsuka: I don't understand anything here.](./IMG_20190817_023514.jpg)
![Soba. I picked up the onsen egg in a spoon and slurped it down in one go. So
smooth.](./IMG_20190817_023704.jpg)

After we got home, I took a shower with my new towel and found it surprisingly
good. Soft, absorbent, and somewhat light. I spent the rest of the night
blogging until we slept well after the sun had come out. I don't know why it
takes me so much time to blog, I hope I get faster at it.

##### Waking hours

1500–2945
