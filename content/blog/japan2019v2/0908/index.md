---
title: Cupnoodles Museum
date: "2019-09-08"
---

I left the house at 1300 to meet Bob and Christine in Yokohama. I was going to
go on a roadtrip with them around the Mt. Fuji area, so I planned to stay one
night with them since I didn't want to commute so far from Tokyo to get to the
car hire the next morning.

_Since I was with Bob and Christine I didn't have as much time to take notes. I
also took a lot of pictures and a lot of them are somewhat self explanatory so
many of them will be missing captions._

![Katsu restaurant in Mark Is shopping center, Minatomirai: the first thing
we did together was eat lunch since it was 1430 at this point. The mortar and
pestle wasn't actually useful for grinding the sesame. The katsu was
okay.](./IMG_20190908_150808.jpg)
![ ](./IMG_20190908_150852.jpg)
![Free salad, miso, and rice refills, although I only had more miso because
it had clams and was delicious.](./IMG_20190908_153911.jpg)
![Tokyu Square, Minatomirai: some drug awareness/anti drug event was on with
performances by schoolchildren.](./IMG_20190908_155559.jpg)
![ ](./IMG_20190908_155715.jpg)
![It had mascots too.](./IMG_20190908_155903.jpg)
![ ](./IMG_20190908_155936.jpg) ![ ](./IMG_20190908_155950.jpg)
![Yokohama Cosmo World: we walked past here on the way to the Cupnoodles
Museum.](./IMG_20190908_160644.jpg)
![That's the Yokohama Landmark Tower. What a silly name.](./IMG_20190908_160741.jpg)
![It's just a little theme park. Hop on a rollercoaster after a hard day at work.](./IMG_20190908_161135.jpg)
![Cupnoodles Museum, Yokohama.](./IMG_20190908_161506.jpg)
![The lobby.](./IMG_20190908_161541.jpg) ![ ](./IMG_20190908_162133.jpg)
![A gallery of cup noodles through history.](./IMG_20190908_162332.jpg)
![ ](./IMG_20190908_162321.jpg)

![Make your own cup noodles!](./IMG_20190908_162622.jpg)
![I'd say it's pretty worth it for 300 JPY considering that's the price of
more expensive cup noodles at the conbini anyway.](./IMG_20190908_162825.jpg)
![Decorating station.](./IMG_20190908_163010.jpg)
![I had no idea what to draw so here's me with Bob and Christine.](./IMG_20190908_165628.jpg)
![There was a card with a few suggestions so I drew the chicken that was in
one of the pictures.](./IMG_20190908_165634.jpg)
![Time to get toppings and seal the cup.](./IMG_20190908_164915.jpg)
![ ](./IMG_20190908_170317.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190908_170450x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Adding a lid and shrink-wrapping.
  </figcaption>
</figure>

![You can also book a session to make your own ramen noodles but we came too
late for that.](./IMG_20190908_165817.jpg)
![A food hall where you can try eight different types of noodles from
different countries that influenced the creator of cup
noodles.](./IMG_20190908_171318.jpg)
![ ](./IMG_20190908_173155.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190908_173917x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    I was amazed when I realised that the image was just made by shadows and not a projector.
  </figcaption>
</figure>

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190908_174148x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Something about getting inspiration from real life... I don't know.
  </figcaption>
</figure>

![ ](./IMG_20190908_173734.jpg) ![ ](./IMG_20190908_174759.jpg)

![Apparently that building in the back is the Intercontinental.](./IMG_20190908_175913.jpg)
![Double decker bike racks.](./IMG_20190908_180727_1.jpg)
![Apartment buildings that actually look nice. I like the blue tinge.](./IMG_20190908_180740.jpg)

I dropped my backpack (with everything for my next few days) off at the hotel,
then we walked to the Nissan Global Headquarters Gallery. On the way, some drunk
Japanese wedding guests were singing loudly in front of us.

![This time we went inside the gallery to have a look.](./IMG_20190908_190653.jpg)
![An accessible car seat that electronically lowers and raises/rotates.](./IMG_20190908_191037.jpg)
![ ](./IMG_20190908_194853.jpg)

![This says something like: all of our shops are closing at 2000 because of
the typhoon approaching tonight.](./IMG_20190908_201309.jpg)

We couldn't find any food outside since everything was closed for the typhoon
(Typhoon Faxai), so we went to the conbini (which never closes I guess) to eat
food in the hotel room. I also noticed that the whole Yokohama Station area
smelt pretty badly like sewage, but I'm not sure if that's just the usual smell
or if it was related to the typhoon.

![Hotel Vista, Minatomirai: not bad.](./IMG_20190908_211452.jpg)
![Not my favourite but still great.](./IMG_20190908_211832.jpg)
![I liked the nori (seaweed) flavour but the texture was like the Thins brand
you get here in Sydney, which I'm not a fan of.](./IMG_20190908_221333.jpg)
![Great flavour! I guess potato chips aren't all bad in Japan. Still
more expensive (~350 JPY) than Sydney though.](./IMG_20190908_223504.jpg)
![Not a fan.](./IMG_20190908_223549-cropped.jpg)

I slept on the floor but it was actually pretty difficult. I didn't realise that
your tailbone makes it hard to sleep on the floor. We all woke up when the
typhoon got serious and the wind and rain became super loud even inside. I
didn't get a video but when we looked outside the wind was crazy and the trees
were swaying wildly.

##### Waking hours

1200–2620
