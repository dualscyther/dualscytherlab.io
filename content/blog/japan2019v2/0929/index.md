---
title: More doing nothing
date: "2019-09-29"
---

When I got up I tried to book sushi restaurants with Elbert but for some reason
most of them went to answering machines and one said that they only accept
bookings for foreigners through a hotel concierge so that they can charge a
cancellation fee easily. I looked up non sushi restaurants in Tokyo and they
seem to be much more accepting for whatever reason.

After all that I left the house at 1630 to eat.

![Mos Burger, Ootsuka: spicy burger with soy patty. The patty was not
noticeably good or bad, so I was satisfied.](./IMG_20190929_164745.jpg)

I took a short break by browsing the local book store, then ate some sushi since
the small burger wasn't anywhere near enough.

![Sushi Misakimaru, Ootsuka: tuna plate. Nothing to write home about but I
was happy with it.](./IMG_20190929_173904.jpg)
![ ](./IMG_20190929_175152.jpg)

![Home, Ootsuka: my throat had been feeling like it had something stuck in it
after eating the burger. When I got home, I coughed out what I assume to be
this prawn whisker.](./IMG_20190929_183113.jpg)

I somehow slept when I got home, then watched the Russian Grand Prix.

![I bought this at FamilyMart. Please be good.](./IMG_20190929_215942.jpg)
![Nothing beats the normal milk puddings, but great nonetheless. Ohayo is
just really good at puddings I guess.](./IMG_20190929_220021.jpg)

Then it was time for dinner.

![A Chinese restaurant, Ootsuka: pretty average, but I like fried rice. The
gyoza was good.](./IMG_20190929_230215.jpg)
![The jellyfish was fishy and there wasn't much of it. Since jellyfish itself
doesn't have a lot of taste, the rest of the dish usually has quite a bit of
flavour but this didn't have much.](./IMG_20190929_230420.jpg)

Doing nothing is one of my favourite activities but with so few days left in
Japan, I do admit that the day felt a bit wasted too.

##### Waking hours

1340–2820
