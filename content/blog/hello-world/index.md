---
title: Hello World!
date: "2019-06-02T22:40:00.000Z"
description: I finally started <del>writing</del> rambling...
---

This is my first blog post on [dtam.me](http://dtam.me). Considering how long it
took me to set everything up even with amazing documentation for
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) and
[Gatsby](https://www.gatsbyjs.org/docs/), I can't imagine how long it would've
taken me if I didn't have a
[starter template](https://github.com/gatsbyjs/gatsby-starter-blog) to go off
of.

Here is my roadmap for this blog:

- get it up and running
- write/post some content. Don't worry too much about the appearance or
  bikeshedding + yak shaving on the technical details
- configure https; right now it's automatically configured on my
  [GitLab subdomain](https://dualscyther.gitlab.io) but not on
  [dtam.me](http://dtam.me)
- make sure the canonical url is set to dtam.me (using rel="canonical" or
  something I think) and/or
  https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-plugin-canonical-urls
- gradually style and customise this blog and yak shave the hell out of the
  technical details
- write more content!

And here's a
[random quote from Paul Graham](http://www.paulgraham.com/good.html) because why
not:

> Do whatever's best for your users. You can hold onto this like a rope in a
> hurricane, and it will save you if anything can. Follow it and it will take
> you through everything you need to do.

I went touristing with family around Sydney the other day, so here's a picture
from Mrs Macquarie's Chair.
![Random picture that I took of Sydney Harbour](./sydney_harbour.jpg)
